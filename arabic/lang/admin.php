<?
$file_rev="041305";
$file_lang="AR";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// admin menu
// titles
$LANG_menu_acct="��������";
$LANG_menu_administration="������ ������";
$LANG_menu_tools="�����";
$LANG_menu_nav="���� ������";

// options..
// accounts
$LANG_menu_valacct="������� ��������";
$LANG_menu_addacct="����� ����";
$LANG_menu_listacct="����� ��������";
$LANG_menu_changedefault="������� ����������";

// administration
$LANG_menu_mailer="����� ���������";
$LANG_menu_categories="����� �������";
$LANG_menu_editpass="����� ���� ������";
$LANG_menu_addadmin="����� / ��� ����";

// tools
$LANG_menu_dbtools="����� ����� ��������";
$LANG_menu_templates="����� �������";
$LANG_editvars_title="����� ��������";
$LANG_menu_editcss="����� ���� ������� ���������";
$LANG_menu_faqmgr="����� ������� �������";
$LANG_menu_checkbanners="���� ��������";
$LANG_menu_editcou="����� ���� ��������";
$LANG_menu_editrules="����� ��������";
$LANG_promo_title="����� ��������";
$LANG_menu_pause="����� �������";
$LANG_menu_unpause="����� �������";
$LANG_commerce_title="����� ��������";
$LANG_updatemgr_title="����� ���������";

// navigation
$LANG_menu_help="��������";
$LANG_menu_home="������ ��������";
$LANG_menu_logout="����� ������";

//Stats Page (/admin/stats.php)
$LANG_stats_nopend="�� ���� ������ ������ .";
$LANG_stats_pend_sing="���� ����� ��� .";
$LANG_stats_pend_plur="���� ����� ��� .";
$LANG_stats_title="���� ������ ��������";
$LANG_stats_statssnapshot="��������";
$LANG_stats_valusr="����� �����";
$LANG_stats_pendusr="����� ������� ��������";
$LANG_stats_totexp="����� ������";
$LANG_stats_loosecred="������";
$LANG_stats_totalban="����� �������� �������";
$LANG_stats_totclicks="����� ������� ��� �������";
$LANG_stats_totsicl="����� ������� �� �������";
$LANG_stats_overrat="���� ������� ������";
$LANG_stats_pendacct="������ ������� ��������";
$LANG_stats_addacct="����� ���� ����";

// Paused message for Stats page
$LANG_exchange_paused="<b>���� ������� ����� ���� !</b> ��� ���� ��� �������� ���������� ���� ����� . ������� �������� �� ���� ������ ��� ����� . ������ ����� ������� , ���� ���� <b>����� �������</b> .";

// Validate account (/admin/validate.php)
$LANG_val_instructions="�������� ������� ������� ��������  . �������� ��� ���� , ���� ��� ���� .";
// $LANG_val_awaiting is for the number of accounts at the bottom
// of the validation page. (eg: "3 account(s) awaiting validation")
$LANG_val_awaiting="������ ������� �������� .";
$LANG_val_noaccts="�� ���� ������ �� ���� ������� �������� !";

// Add Account (/admin/addacct.php)
// uses some of the edit account form/lang files
$LANG_addacct_title="����� ���� ����";
$LANG_addacct_msg="�� ����� ������ ����� !";
$LANG_addacct_button="��������";

// The following are shared between the Add Account,
// edit account and validate account pages.
$LANG_edit_realname="��� �� ������ ";
$LANG_edit_login="��� �������� ";
$LANG_edit_pass="���� ������ ";
$LANG_edit_email="������ ���������� ";
$LANG_edit_category="����� ";
$LANG_edit_nocats="�� ��� ������� ������ �� ��� , ���� �� ����� ����� ����� ������ .";
$LANG_edit_exposures="��������� ";
$LANG_edit_credits="������ ";
$LANG_edit_clicks="������ ��� ������ ";
$LANG_edit_siteclicks="������ �� ������ ";
$LANG_edit_raw="��� ������ ������� ";
$LANG_edit_status="���� ������ ";
$LANG_edit_approved="  ��� ";
$LANG_edit_notapproved="  ������� �������� ";
$LANG_edit_defaultacct="���� ������� ";
$LANG_edit_sendletter="����� ����� ������� ";
$LANG_edit_button_val="����� ������";
$LANG_edit_button_reset="����� �����";
$LANG_edit_button_delraw="��� HTML ������ �������";
$LANG_edit_button_del="��� ����";

// Edit Account (/admin/edit.php)
$LANG_edit_title="����� ����";
$LANG_edit_heading="����� / ����� ������";
// send e-mail link to the right of the e-mail field
$LANG_email_button_send="����� ���� ��������";
$LANG_edit_saleshist="���� ������ �������";
// for viewing raw mode HTML
$LANG_edit_raw_current="������";
$LANG_edit_button_addban="����� �����";
$LANG_edit_button="������ ��� ��������";
$LANG_edit_bannerlink="��� / ����� ��������";
$LANG_stats_banner_hdr="��������";
$LANG_stats_hdr_add="����� �����";
// Banner report at the bottom of the edit page
// eg: "3 active banners found for [accountname]"
$LANG_edit_banners="����� ���� ���� ����";

// Edit/validate Confirm message:
$LANG_editconf_msg="�� ����� ������ ����� !";
$LANG_valconf_msg="�� ����� ������ ����� !";

// Banners (/admin/banners.php)
$LANG_targeturl="���� ����� ";
$LANG_filename="��� ����� ";
$LANG_views="������ ";
$LANG_clicks="������� ";
$LANG_bannerurl="���� ������� ";
$LANG_menu_target="����� ������";
$LANG_button_banner_del="��� �������";
$LANG_banner_instructions="������ ���� ������� �� ������ ����� , ��� �������� �� ������ ������� , �� ���� �� <b>����� ������</b> . ���� ����� , ���� �� <b>��� �������</b> . ������ ������ ������ �� ��� ���� ������ , ���� ��� ������� ������� �� .";

// this variable displays at the bottom of the "Banners" page.
// eg: "4 banner(s) found for this account"
$LANG_banner_found="����� ���� ���� ������ .";
$LANG_stats_nobanner="�� ���� ������ ���� ������ !";

// Delete Banner (/admin/deletebanner.php)
$LANG_delban_title="��� �������";
$LANG_delban_verbage="�� ��� ����� �� ��� ��� ������� � �� ���� ������� �� ��� ������ .<br>";
$LANG_delban_go="��� , ���� ��� �������";
$LANG_delbanconf_verbage="�� ��� ������� ����� !";

// Account Listing page (/admin/listall.php)
$LANG_listall_title="����� ����� ��������";
$LANG_listall_button_back="������ ��� ����������";
$LANG_listall_default="�������� ����������";
$LANG_listall_nodef="�� ���� ������ �������� .";
$LANG_listall_def_sing="���� ������� ��� .";
$LANG_listall_def_plur="���� ������� ��� .";
$LANG_listall_nonorm="�� ���� ������ ����� .";
$LANG_listall_norm_head="�������� ������ �������";
$LANG_listall_norm_sing="���� ���� ��� .";
$LANG_listall_norm_plur="���� ���� ��� .";

// Delete Account Page (/admin/deleteaccount.php)
$LANG_delacct_title="��� ������";
$LANG_delacct_verbage="�� ��� ����� �� ��� ������ ������ �����";
$LANG_delacct_go="��� , ���� ������";
$LANG_delacct_done="�� ��� ������ ������ ����� �";

// Default Banner (/admin/changedefaultbanner.php)
$LANG_menu_changedefault="������� ����������";
$LANG_changedefault_title="����� ������� ����������";
$LANG_changedefault_message="��� ������� ������ ������� ���������� ���� ���� ����� �� ����� ������ ���� ���� �� . ������� ���������� �� ���� ������ ��������� . ��� �� ������ ��������� ��� ����� . ������� ���������� ������� �� �";
$LANG_changedefault_url="������ ����� ";
$LANG_changedefault_nodefault="�� ��� ����� ����� �������� ��� ���� !";
$LANG_changedefault_bannerurl="���� ������� ";

// Email All Accounts (/admin/email.php)
$LANG_email_title="������ ���� ��������";
$LANG_email_override="����� ������ �����";
$LANG_email_override_warning="������� ��� ������ *��������*";
$LANG_email_message="������� �<p>���� ��� �������� ������ ����� ������� ����� ��������� . ��������� ������� ";
$LANG_email_button_reset="����� �����";
$LANG_email_address="������ ���������� ";
$LANG_email_user="������ ��� ";
$LANG_email_senttouser="�� ����� ������� ��� ";
$LANG_email_return="���� �����</a> ������ ��� ���� ����� ������ .";
$LANG_email_allcats="���� �������";

// Email Sending status page (/admin/emailgo.php)
$LANG_emailgo_title="������ ���� ������� ";
$LANG_emailgo_msg_all="������ <b>���� �������</b> ( ��� ��� ����� ����� ��� �������� �� ������� �������� ) ... ��� ���� ���� ��� ���� .";
$LANG_emailgo_msg_only="����� ����� ������� ������� ��������� �� ������� ... ��� ������� ���� ��� ����� .";

// Email confirmation page (/admin/emailsend.php)
$LANG_emailconf_title="������ ���� ������� - �����";
$LANG_emailconf_subject="������� ";
$LANG_emailconf_msg="������� ";
$LANG_emailconf_button_send="���� �������";
$LANG_emailconf_button_reset="����� �����";

// Category Admin (/admin/catmain.php)
$LANG_catmain_title="����� �������";
$LANG_catmain_header="������� �������";
$LANG_catmain_catname="��� �����";
$LANG_catmain_sites="�������";
$LANG_catmain_addcat="����� ��� ";
$LANG_catsfound_singular=" ��� ��� .";
$LANG_catsfound_plurl=" ��� ��� .";

// Delete Category (/admin/delcat.php)
$LANG_delcat_title="��� �����";
$LANG_delcat_acctexist="���� ������ <b>$get_count</b> ���� �� ��� ����� . ��� ��� ����� ����� ��� ��� ���� ������� ��� ����� ��������� . �� ��� <b>* ����� *</b> �� ��� ���� �������� �";
$LANG_delcat_sure="�� ��� ����� �� ��� ��� ����� �";
$LANG_delcat_button="��� / ���� �����";

// Delete Category Confirmation (/admin/delcatconf.php)
$LANG_delcatconf_reset="��� ���� �������� �� ����� ������ ��� ����� ��������� ( ���� ���� ��� ����� , ���� ������ <b>$get_num</b> ���� ����� ������� )";
$LANG_delcatconf_status="����� ����� ������ <b>$name</b> (id: <b>$id</b>)";
$LANG_delcatconf_success="�� ��� ����� ����� !";

// Edit Category (/admin/editcat.php)
$LANG_editcat_title="����� �����";
$LANG_editcat_catname="��� ����� ";
$LANG_editcat_success="�� ����� ����� ����� !";

// Add Admin page (/admin/addadmin.php)
$LANG_addadmin_title="����� / ����� ���� �����";
$LANG_addadmin_list="��������� �������� ";
$LANG_addadmin_newlogin="��� �������� ";
$LANG_addadmin_pass1="���� ������ ";
$LANG_addadmin_pass2="����� ���� ������ ";

// Change Password page (/admin/editpass.php)
$LANG_editpass_title="����� ���� ���� �������";
$LANG_editpass_newpass="���� ������ ������� ";
$LANG_editpass_newpass1="����� ���� ������ ";
$LANG_editpass_button="����� ���� ������";
$LANG_editpass_reset="����� ����� ������";

//Password confirmation page (/admin/pwconfirm.php)
$LANG_pwconfirm_title="����� ���� ������";
$LANG_pwconfirm_success="�� ����� ���� ������ ����� ! ���� <a href=\"index.php\">��� ������ ��� ����� ������</a> ������ ������ �������� ���� ������ ������� .";

// Database tools.. (/admin/dbtools.php)
$LANG_db_title="����� ����� ��������";
$LANG_db_buname="������ ����������";
$LANG_db_budate="����� �������";
$LANG_db_delete="���";
$LANG_db_backupfiles="����� ����� ���������";
$LANG_db_newbuset="����� ���� �������� �����";
$LANG_db_instructions="������ ���� �������� ����� ���� ���� \"����� ���� �������� �����\" ( �� ������ ��� ����� ����� ���� ����� �������� ) . ���� ��� ��� ��� ������ ������� ��������� ��� �������� . ���� ��� / ��� �� ��� ���� �� �������� ������ ��� ����� .<p><b>����� ! �</b> ������� �� ���� ����� ��������� ���� ������� � ������� , ��� ������� ���� ������ ����� ���� ����� ����� ������� ( ��� ��� .htaccess ) ! , ���� �� ��� ����� ����� ��������� ��� ������� .";
$LANG_db_restore="  �������  ";
$LANG_db_upload="��� ���� ��������";
$LANG_db_upload_button="��� ������ ����";

// Edit Templates (/admin/templates.php)
$LANG_menu_templates="����� �������";
$LANG_templates_message="��� ������ ���� ���� ������ �� ������� �������� ������� . ���� ����� ������ ��� ����� ��� . ����� ����� �� ������� , ���� �� ��� ��������� .";
$LANG_templates_warning="����� � ��� ��������� ��� ����� ����� ��� ������� , � �� ���� ������ ���� �� ��� �� ���� �� ����� ! �� ���������� ��� �������� !";
$LANG_template_box="������ ";
$LANG_template_choose="����� ���� ���� ...";
$LANG_preview="������";
$LANG_valid_tags="����� ������� � ";

// Edit variables page (/admin/editvars.php)
$LANG_editvars_title="����� ���������";
$LANG_varedit_dirs="����� ������� ������ , ���� ����� ���� ������ ���� ��� ���� ������� , ��� �������� , ���� ������� , ��� . ������ �� ��������� ���� <a href=\"../docs/install.php\">���� ������� �������</a> .";

$LANG_dbinstall_head="������� ����� ��������";
$LANG_varedit_dbhost="���� ����� �������� ";
$LANG_varedit_dblogin="��� ������ ����� �������� ";
$LANG_varedit_dbpass="���� ���� ����� �������� ";
$LANG_varedit_dbname="��� ����� �������� ";

$LANG_pathing_head="�������� & ������� �������";
$LANG_varedit_baseurl="���� ��� ����� �������� ";
$LANG_varedit_baseurl_note=" ���� ���� ����� �� ������� ";
$LANG_varedit_basepath="������ ������� ";
$LANG_varedit_exchangename="��� �������� ";
$LANG_varedit_sitename="��� ������ ";
$LANG_varedit_adminname="��� ������� (����� ����� ) ";
$LANG_varedit_adminemail="������ ���������� ";

$LANG_banners_head= "��������";
$LANG_varedit_width="��� ������� ������� �� ";
$LANG_varedit_height="������ ������� ������� �� ";
$LANG_varedit_pixels=" ����";
$LANG_varedit_defrat="���� ������� ( ��� ������ ) ";
$LANG_varedit_showimage="��� ���� �������� � ";
$LANG_varedit_imageurl="���� ���� �������� ";
$LANG_left="  ����  ";
$LANG_right="  ����  ";
$LANG_top="  ����  ";
$LANG_bottom="  ����  ";
$LANG_varedit_imageurl_msg=" ����� ���� ������ ";
$LANG_varedit_showtext="��� ���� ��������� � ";
$LANG_varedit_exchangetext=" ���� ������� ������ ";
$LANG_varedit_reqapproval="��� �������� ��� ������� � ";
$LANG_varedit_upload="������ ������ ������� ��� ������ � ";
$LANG_varedit_maxsize="��� ������� ������� �� ( ������ ) ";
$LANG_varedit_uploadpath="���� ��� �������� ( ���� ���� ����� ) ";
$LANG_varedit_upurl="���� ���� ������� ";
$LANG_varedit_maxbanners="��� �������� ������ ��� ��� ";

$LANG_anticheat="������ ����";
$LANG_varedit_anticheat="����� ��� ���� ";
$LANG_varedit_cookies="�������";
$LANG_varedit_db="����� ��������";
$LANG_varedit_none="����";
$LANG_varedit_duration="������ ";
$LANG_varedit_duration_msg="  ����� ...";

$LANG_referral_credits="��������� & ���������";
$LANG_varedit_referral="������ ��������� ";
$LANG_varedit_bounty="������ �������� ��� ����� ";
$LANG_varedit_startcred="������ �������� ����� ������ ";
$LANG_varedit_sellcredits="��� ������ ";

$LANG_misc="������ ����";
$LANG_varedit_topnum="��� ���� X ���� ";
$LANG_varedit_topnum_other=" ���� ";
$LANG_varedit_sendemail="���� ����� ������� ���������� ��� ����� ��� ���� ";
$LANG_varedit_usemd5="������� ���� MD5 ������ ���� ������ ( ����� ��� ����� ������� ) ";
$LANG_varedit_usegz="������� GZip ���� ������� ( ���� ������ �� ���� ������� � ���� ���� ������ ) ";
$LANG_varedit_userand="������� mySQL4 rand(): ( ��� ������ ���� �� ���� exchange ����� �������� �������� � ���� ����� ����� mySQL 4.x �� ���� ) ";
$LANG_varedit_logclicks="����� ������� ( ����� ��� � ����� � IP �� ���� ��� ������� �� ����� �������� , � ��� ���� ���� �� ��� ����� �������� ) ";
$LANG_varedit_userandwarn="����� mySQL 4 �� ���� ";
$LANG_date_format="����� ������� ";

// Edit Style Sheet (/admin/editcss.php)
$LANG_editcss_directionstop="��� ���� ������� ��������� ���� ���� ��������� �� �������� , �� ���� �� ( ���� ) . ����� ��������� ���� ����� . �� �� ����� ���� ������� ���� ���� ������� , �� ������ ��� ������ /template/css �� ������ ����� �������� .";
$LANG_editcss_instructions1="��� ���� ������� ��������� ���� ���� ������� �� ������� ������� , �� ���� �� ( ���� ) . ���� ����� ������ . �� ���������� �������� , �� ���� �� ( ���� ) ���� ������ ����� . ��� ����� ���� ����� ����� ( �� �� ��� ���� �� ��� ) ��� ��� ��������� ������ ������� ������ !";
$LANG_editcss_loadbutton="����� ���� CSS �������";

// FAQ Manager (/admin/faq.php)
$LANG_faq_found="���� ����� ��� .";
$LANG_faq_add="����� ��� ����";

// Check Banners (/admin/checkbanners.php)
$LANG_checkbanners_description="��� ������ ����� ������� ���� ���� �������� � ���������� ������� �� ������ ��������� . ��� ������ ����� ������ �������� ����� ������� � �������� �������� ������ �� ����� �������� . ��� ��� ������ �� ������� �� �������� , ���� ��� ������ �������� �� ���� ������� � ������ �� ����� �������� ������� �������� . ����� ����� ����� ����� ��� �������� ��� ������ �� ���� ������� ������ ������� .<p>���� ������ �������� ���� ������ <b>������ ��������</b> ������ . ������ � ��� ������� ��� ����� ��� ��� ������ �������� �� �������� .";
$LANG_status_OK=" ���� ";
$LANG_status_broken=" ����� ";

// Edit COU/Rules (/admin/editstuff.php)
$LANG_editstuff_message="����� ������� ��� HTML . ���� ��� ��������� ������ ������ ��� ������� ��������� .<p> ��������� ������� �";
$LANG_editstuff_result="�� ����� ������ ����� !";
$LANG_editstuff_url="���� �����</a> ������� ��������� !";
$LANG_exchange_paused="<b>���� ������� ����� ���� !</b> ��� ���� ��� ��� �������� ���������� ���� ����� ��� . ����� ������ ������� �� ��� ������ . ������ ����� ���� ������� , ���� ���� <b>����� �������</b> �� ������� �������� .";

// Promo/Coupon Manager (/admin/promos.php)
$LANG_promo_title="����� �������";
$LANG_promo_noitems="�� ���� ������ ����� ����� ���� . ����� ����� ���� �� ����� �������� ������� ������ .";
$LANG_promo_history="���";
$LANG_promo_name="��� �������";
$LANG_promo_code="�����";
$LANG_promo_type="�����";
$LANG_promo_credits="������";
$LANG_promo_status="������";
$LANG_promo_timestamp="����� �����";
$LANG_promo_type1="���� ������";
$LANG_promo_type2="�� �����"; // percentage off item. expressed as xx% off item.
$LANG_promo_type3="��� ���";
$LANG_promo_add="����� �����";
$LANG_promo_value="������ ( ��� ����� )";
$LANG_promo_reuse="���� ����� ����� ������� ��� ������� ";
$LANG_promo_reuseint="���� ����� ��������� ";
$LANG_promo_reusedays="��� ";
$LANG_promo_usertype="��� ������� ��������";
$LANG_promo_newonly="������� ����� ���";
$LANG_promo_all="���� �������";
$LANG_promo_listall="����� �����";
$LANG_promo_listdel="����� ������";
$LANG_promo_listact="����� ����";
$LANG_promo_deleted="������"; // Status of promo displayed in name field
$LANG_promo_active="����";

// Promo Details.. (/admin/promodetails.php)
$LANG_promodet_title="����� - ��� ��������";
$LANG_promodet_overview="����� �������";
$LANG_promodet_loghead="��� ���������";
$LANG_promodet_nostats="�� ���� ����� ������� . ���������� ���� ����� ��� ������� ������� �� ��� ����� .";
$LANG_promodet_id="��� �������";
$LANG_promodet_name="��� �������";
$LANG_promodet_type="��� �������";
$LANG_promodet_code="��� �������";
$LANG_promodet_vals="���� �������";
$LANG_promodet_credits="���� �������";
$LANG_promodet_reuse="���� ����� ����� ���������";
$LANG_promodet_reuseint="��� ������ ����� ���������";
$LANG_promodet_reuseintdays="���";
$LANG_promodet_usertype="��� �������";
$LANG_promodet_timestamp="����� �������";
$LANG_promodet_status="���� �������";
$LANG_promodet_usedate="����� ���������";

// Store Manager (/admin/commerce.php)
$LANG_commerce_title="����� ��������";
$LANG_commerce_noitems="�� ���� ������ �� ��� ���� �� �������� . ����� ����� ���� ����� �������� ������� ������ .";
$LANG_commerce_name="��� ����� ";
$LANG_commerce_credits="������ ";
$LANG_commerce_price="����� ";
$LANG_commerce_purchased="������";
$LANG_commerce_options="������";
$LANG_commerce_add="����� ���";
$LANG_commerce_edititem="����� ���";
$LANG_commerce_filterhead="�����";
$LANG_commerce_recordsperpage="��� ������� ";

$LANG_commerce_view="��� / ��� �� ���������";
$LANG_commerce_notrans="�� ���� ������� ����� ����� ���� . ��� ���� ��� ���� ������� �� ��� ���� ������� ������ ������ ���� ������ .";
$LANG_next="������";
$LANG_previous="������";
$LANG_commerce_invoice="������";
$LANG_commerce_user="���";
$LANG_commerce_item="���";
$LANG_commerce_status="�������� �������";
$LANG_commerce_payment="���� �������";
$LANG_commerce_email="���� ������";
$LANG_commerce_date="�������";

$LANG_commerce_filterorders="��� ������� �� ���������� ";
$LANG_commerce_uidsearch="��� �� ����� ���� ��� ����� ";
$LANG_commerce_go="���� !";
$LANG_commerce_reset="����� / ����� ����� �������";

// Login Page (/admin/index.php)
$LANG_index_title="���� ���� ������ ��������";
$LANG_index_msg="Banner Exchange<br>���� ������ ��������";
$LANG_index_login="������";
$LANG_index_password="���� ������";

// Logout Page (/admin/logout.php)
$LANG_logout_title="����� ������";
$LANG_logout_msg="�� ����� ����� ����� !<p><a href=\"index.php\">���� ������</a> ������ ��� ���� ����� ������ .";

// Update page (/admin/update.php)
$LANG_updatemgr_title="����� ���������";
$LANG_updatemgr_inst="���� ��������� ���� �� ����� ���� ����� ������� phpBannerExchange 2.x  � ������ ����� . ��� ���� ���� ��������� ���� ���� , ��� ������ �� �� ����� \"manifest.php\" ���� ������� ( ������� 777 ) . ��� ����� ���� �� ������ ����� �������� . ���� ����� ������ �������� �������� ���� �� ����� ������� . ���� ������ ��� �������� ������� �";
$LANG_updatemgr_full="����� ����� / ����� ����";
$LANG_updatemgr_fulldesc="����� ����� Manifest �� ��� ����� ����� ������ � ������ �� ��������� ��� ������ ������� . ( ���� ���� ) .";
$LANG_updatemgr_refresh="����� ����� Manifest";
$LANG_updatemgr_refreshdesc="����� ����� Manifest �� ��� ����� ����� ������ ��� .";
$LANG_updatemgr_updonly="����� ���";
$LANG_updatemgr_updonlydesc="������ ��� ��������� ���� ����� ����� Manifest . ��� ���� ����� Manifest ���� ���� ������� , ��� ����� ��� ��������� !";
$LANG_updatemgr_checkperms="������ �������� ��� ����� <b>manifest.php</b> ...";
$LANG_updatemgr_permok="����� <b>manifest.php</b> ���� ������� . ������ ...";
$LANG_updatemgr_manifestcheck="���� ����� ����� ...";
$LANG_updatemgr_manifeststamp="��� ��� �� ����� manifest .";
$LANG_updatemgr_never="�� ����";
$LANG_updatemgr_url="����� ������";
$LANG_updatemgr_popmanifest="����� manifest . ���� �������� ...";

// update manager errors (can't put these in the errors.php file)..
$LANG_updatemgr_permerror="<b>��� !</b> ����� <b>manifest.php</b> ��� ���� ������� ������ ������� ! ��� ���� ��� �� ���� ����� manifest ! . ���� �� ��� ������� ���� ����� �� ��� �������� ( ���� ����� manifest.php ������� 777 ) .";
$LANG_updatemgr_nomanifest="<b>��� !</b> ���� ������ ��� ����� <b>manifest.php</b> ������ ������� ! ���� ����� manifest.php ����� �� ������ ����� �������� phpBannerExchange �� ���� ������ .";
$LANG_updatemgr_successwrite="�� ����� ����� <b>manifest.php</b> ����� ! . ������ ...";
$LANG_updatemgr_getmaster="������ ��� ����� ������ ������� . �� ������ ��� ����� .";

$LANG_updatemgr_cantconnect="���� ��� ������� ������� ������� ������ . �� ���� ��� ��� �� ����� PHP ���� �� ���� ��� ������� �� �� ���� ����� ��� ����� \"<b>allow_url_fopen</b>\" ������� ������� . � �� ���� ����� ���� �� ������ ��� ���� ������ . ��� ������ ��� ������� , ���� ����� ����� ������ �� ��� ������� ����� PHP 4.3.0 �� ���� �� �� ������� ������� <b>allow_url_fopen</b> ����� �� ������� PHP .";
$LANG_updatemgr_compare="������ ������� ������� .. ���� �������� ...";
$LANG_updatemgr_uptodate="��� �������";
$LANG_updatemgr_needsupdt="����� �����";
// $number files found on the master list.
$LANG_updatemgr_valsfound="��� ��� �� ������� ������� .";
$LANG_updatemgr_notupgrade="���� ������ ������ ! �� ���� ����� ������� ������� .";
$LANG_updatemgr_updwaiting="��� ������� ������� . ������� ������� ������ � �� ����� ������� �";
$LANG_updatemgr_updateinst="������ ��������� , ������ �� ���� ������� ��� ����� , �� ��� ������� �� .txt ��� .php , �� �� ���� ������� ��� ������ ������ ��� ����� . ��� ����� �������� ( ���� � \"/index.php\" ���� �� ���� ����� \"index.php\" �� ������ ����� �������� , \"/admin/validate.php\" �� ����� \"validate.php\" ����� ������ \"/admin\" , ��� ) .";
$LANG_updatemgr_changelog="��� ���������";

// Warnings..
$LANG_installdir_warning="����� ���� ! � ������ install �� ���� ����� ! � ��� ���� ��� ���� ���� . �� ������� ���� ������ ����� ����� � ���� ! ( �� ����� ���� ��� �� �� ����� ������ � ��� ����� ��� ����� ) ����� ����� ! .";

// Generic words used all over the place
$LANG_yes="���";
$LANG_no="��";
$LANG_back="����";
$LANG_submit="����";
$LANG_reset="����� �����";
$LANG_emailing="������ ";
$LANG_done=" ��� ";
$LANG_ID="�����";
$LANG_question="������ ";
$LANG_action="�������";
$LANG_delete="���";
$LANG_edit="�����";
$LANG_answer="������ ";
$LANG_reactivate="����� �����";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!

?>