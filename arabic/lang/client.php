<?
$file_rev="041305";
$file_lang="ar";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

//Common messages
//menu:
$LANG_menu_nav="��������";
$LANG_menu_home="������ ��������";
$LANG_menu_logout="����� ������";

$LANG_menu_stats="����������";
$LANG_menu_emstats="���� ���������� �������";

$LANG_menu_site="������ �����";
$LANG_menu_commerce="���� ������";
$LANG_menu_banners="����� ��������";
$LANG_menu_cat="����� �����";
$LANG_menu_htmlcode="������ ��� ��� HTML";

$LANG_menu_info="����� �������";
$LANG_menu_changeem="����� ������ ����������";
$LANG_menu_changepass="����� ���� ������";
$LANG_coupon_menuitem="����� ��� Promo";

//Common stuff
$LANG_reset="����� �����";

// Stats Page (client/stats.php)
$LANG_stats_title="���� ���� �����";
  //Stats Window stuff
$LANG_stats_startdate="����� �������";
$LANG_stats_siteexpos="������ ���� �� �����";
$LANG_stats_siteclicks="������ ��� �����";
$LANG_stats_percent="������ �������";
$LANG_stats_ratio="���� �������";
$LANG_stats_exposures="���� ������";
$LANG_stats_avgexp="���� ���� ������ / ������";
$LANG_stats_clicks="��� �������� ��� �����";
$LANG_commerce_credits="������";
  // Explanation of stats Window Stuff. These next 2 groups
  // are separate even though they say roughly the same thing
  // to keep them easier to manage.
  //
  // This first one is for a normal x:1 ratio site..Full message
  // reads: "To date, you have displayed [x] banners on your site,
  // and generated [y] clicks from those exposures. You have earned
  // [z] credits for your own banner on other sites (you earn [n]
  // exposure(s) for each banner you display)."
$LANG_stats_exp_normal="<br>��� ����� , �� ���";
$LANG_stats_exp_normal1="����� �� ����� , � �� �� �����";
$LANG_stats_exp_normal2="����� �� ���� ����� .  � �� ���� ���";
$LANG_stats_exp_normal3="���� ���� ������� �� ����� ������ ( ���� ���";
$LANG_stats_exp_normal4="���� ������� ����� �� ����� ���� �� ����� ) .";
  // This one is for "odd" ratio sites like 5:4 ..Full message
  // reads: "To date, you have displayed [x] banners on your site,
  // and generated [y] clicks from those exposures. You have earned
  // [z] credits for your own banner on other sites (you earn [n]
  // exposure(s) for each [n] displays)."
$LANG_stats_exp_weird="<br>��� ����� , �� ���";
$LANG_stats_exp_weird1="����� �� ����� , � �� �� �����";
$LANG_stats_exp_weird2="����� �� ���� ����� .  � �� ���� ���";
$LANG_stats_exp_weird3="���� ���� ������� �� ����� ������ ( ���� ���";
$LANG_stats_exp_weird4="���� ������� ����� �� <b>$banexp</b> ����� ���� �� ����� . )";
  // Approved or not approoved messages.
$LANG_stats_approved="���� ������ � ��� .";
$LANG_stats_unapproved="����� ���� ������� ������ ������� . ���� ����� ����� �� ���� ������� ��� ���� �������� ���� , ����� ����� ���� ������ ��� �� ��� ��� .";
$LANG_stats_bannercount="����� �������� �";
 // Referral messages. Reads: "You have earned [x] credits by
 // referring [y] account(s) to the exchange. Currently, there
 // are [z] accounts referred by you awaiting validation..(etc)".
$LANG_stats_referral1="��� ���� ���";
$LANG_stats_referral2="���� ����� ������";
$LANG_stats_referral3="��� ��� ������ ������� . ������ ����";
$LANG_stats_referral4="���� �� ������ �� ����� ������� �������� . ������ ������� ����� ��� ��� ������ ������� ��� �������� ���� �� ������� .";
  // Tips
$LANG_tip_startdate="����� ����� �����";
$LANG_tip_siteexposure="��� �������� ���� ���� �� �����";
$LANG_tip_clickfrom="��� ������� ��� �������� ���� ���� �� �����";
$LANG_tip_percentout="������ ������� ����� ����� ����� ����� ��� �������� �� ����� ������ ����� ������� �������";
$LANG_tip_ratioout="���� ���� ����� ����� ����� ��� �������� �� ����� ������ ����� ������� �������";
$LANG_tip_exposures="��� ������ ���� ���� ���� ������� �� ����� ������� �������";
$LANG_tip_avgexp="������ ������ ����� ������� �� ����� ������� �������";
$LANG_tip_clicks="��� ������ ���� �� ����� ���� ��� ������� �� ����� ������� ������� .";
$LANG_tip_percentin="������ ������� ����� ����� ������� ������� ����� ����� ��� ������� .";
$LANG_tip_ratioin="���� ���� ����� ������� ������� ����� ����� ��� ������� .";
$LANG_tip_credits="����� ������ ������� �� ����� � ����� ������� , ���� ���� ����� �� ���� ��� �������� �� ����� .";

// Log Out page (/client/logout.php)
$LANG_logout_title="����� ������";
$LANG_logout_message="�� ����� ����� ����� !<p><a href=\"../index.php\">���� ������</a> ������ ��� ���� ����� ������ .";

// Email Stats (/client/emailstats.php)
$LANG_emailstats_title="���� ���������� ��� ������ ����������";
$LANG_emailstats_msg="�� ����� ����� ���� �������� ��� $email ����� �������� ����� .";

// Commerce/Buy Credits (/client/commerce.php)
$LANG_commerce_noitems="�� ���� ������ ���� ����� ������";
$LANG_commerce_name="��� �����";
$LANG_commerce_price="�����";
  // "Buy now via [service]". In the future, phpBannerExchange
  // will support multiple payment services.
$LANG_commerce_buynow_button="������ ���� ������";
$LANG_commerce_buynow="������ ����";
$LANG_commerce_history="��� ��������";
$LANG_commerce_date="�������";
$LANG_commerce_item="�����";
$LANG_commerce_purchaseprice="��� ������";
$LANG_commerce_invoice="��������";
$LANG_commerce_nohist="�� ���� �� ������ !";
$LANG_commerce_couponhead="�������";
$LANG_commerce_coupon_button="����� �������";

// Banners (/client/banners.php)
$LANG_targeturl="������ ����� ";
$LANG_filename="��� ����� ";
$LANG_views="������ ";
$LANG_clicks="������� ";
$LANG_bannerurl="���� ������� ";
$LANG_menu_target="����� ������";
$LANG_button_banner_del="��� �������";
$LANG_stats_hdr_add="����� �����";
$LANG_banner_instructions="������ ���� ������� �� ������ ����� , ��� �������� �� ����� ������� , �� ���� �� <b>����� ������</b> .  ���� ����� , ���� �� <b>��� �������</b> .  ������ ������ ������ �� ��� ������ ����� , ���� ��� ������� ������� �� .";

// this variable displays at the bottom of the "Banners" page.
// eg: "4 banner(s) found for your account"
$LANG_banner_found="����� ���� �� ����� .";
$LANG_stats_nobanner="�� ���� �� ����� �� ����� !";

// Delete Banner (/client/deletebanner.php and /client/deleteconfirm.php)
$LANG_delban_title="��� �����";
$LANG_delban_warn="�� ��� ����� �� ��� ��� ������� � �� ����� ������� �� ��� ������� .<br>";
$LANG_delban_button="��� , ���� �������";
$LANG_delbanconf_verbage="�� ��� ������� ����� !";
$LANG_delbanconf_success="�� ����� ������� �� ����� ����� .";

// Category page (/client/category.php and /client/categoryconfirm.php)
$LANG_cat_reval_warn="����� ����� ����� ������ ������� . ( ����� �� ��� ������ ������ �� ��� ����� ������� �������� ) .";
$LANG_cat_change_button="����� �����";
$LANG_cat_nocats="�� ��� ������� ������ �� ��� ������ , ���� �� ����� ����� ����� ���� .";
$LANG_catconf_message="�� ����� ����� ����� !";

// Get HTML (/client/gethtml.php)
$LANG_gethtml_title="������ ��� ��� HTML";
$LANG_gethtml_message="��� ��� ���� �� ������� ���� ����� ���� �������� �� ����� ������ �� ����� , �� \"&cat=0\" �� ��� ����� ����� �� ������ ������ ( ���� � \"&cat=2\" ,  \"&cat=3\" , ��� ) . ���� �� ������� ��� ����� �� ��� ������� . ����� ������ ��� ������ ���� ���� ��� ��� ����� �� ����� . ������� ��� ������ ��� �� ����� �������� �� ���� ������� ��� ����� �� ����� ������ �� ����� .";
$LANG_gethtml_catname="��� �����";
$LANG_gethtml_catid="��� �����";

// Change email address (/client/editinfo.php)
$LANG_email_title="����� �����";
$LANG_email_address="������ ���������� ";
$LANG_email_button="����� ������� ����";

// Change email confirmation (/client/editconfirm.php)
 $LANG_infoconfirm_title="����� �����";
 $LANG_infoconfirm_success="�� ����� ���� ���������� ��� ";

// Change PW form (/client/editpass.php)
$LANG_pass_title="����� ���� ������";
$LANG_pass1_label="���� ������ ������� ";
$LANG_pass2_label="����� ���� ������ ";
$LANG_pass_button="��� ���������";
$LANG_pass_confirm="�� ����� ���� ������ ����� ! ����� ���� ��� <a href=\"logout.php\">����� ������</a> �� ������ �������� ���� ������ ������� .";

// Buy Credits page (/client/promo.php)
$LANG_coupon_menuitem="���� ��� �������";
$LANG_coupon_instructions="���� ��� ������� ���� ������ �� ������� .";
$LANG_submit="����";
$LANG_coupon_success="<b>�� ������� ������� ����� !</b>";
$LANG_coupon_success2="�� ����� $credits ���� ��� ����� � ����� ����� �������� ������ .";

// Click Log (/client/clicklog.php)
$LANG_clicklog="��� �������� ";
$LANG_clicklog_from="�� ����� ";
$LANG_clicklog_to="��� ����� ";
$LANG_clicklog_ip="����� IP ";
$LANG_clicklog_date="����� � ������� ";
$LANG_noclicks="�� ���� ����� �� ����� ����� .";


//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>