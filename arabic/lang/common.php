<?
$file_rev="041305";
$file_lang="AR";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Menu items..
$LANG_menu_options="��������";
$LANG_backtologin="����� ������";
$LANG_lostpw="���� ���� ������ �";
$LANG_faq="������� ��������";
$LANG_signup="�������";
$LANG_rules="���� �������";
$LANG_tocou="���� ��������";

$LANG_menu_extras="������ ������";
$LANG_topbanns="���� �������";
$LANG_overallstats="�������� ����";

// login page (/index.php)
$LANG_indtitle="������ ��� ���� ������";
$LANG_headertitle="���� ���� �����";
$LANG_login_instructions="���� ��� �������� � ���� ������ ������ ��� ����� . ��� ��� ���� �������� , <a href=\"cou.php\">���� ������</a> ������ ���� ���� !";
$LANG_login="��� �������� �";
$LANG_pw="���� ������ �";
$LANG_login_button="����� ������";

//Recover Password (/recoverpw.php)
$LANG_lostpw_title="������� ���� ������";
$LANG_lostpw_instructions="������ ����� ���� ������ , ���� ������ ���������� ������ ����� . ���� ����� ���� ���� ����� � ������� �� ��� ����� ���������� ";
$LANG_lostpw_instructions2=" .";
$LANG_lostpw_recover="�� ����� ����� ���� ������ � ��� ����� ���� ������ ������ ����� ��� ����� ���������� .";
$LANG_lostpw_email="������ ���������� ";
$LANG_lostpw_success="�� ����� ����� ���� ������ ! ��� ����� ���� ������ ������� ��� ����� ���������� . ����� ����� ���� ������ �� ���� ������ ��� ����� .";

// Signup Form (/signup.php)
$LANG_signupwords="������� �� $exchangename";
$LANG_realname="����� ������ ";
$LANG_pw_again="����� ���� ������ ";
$LANG_cat="����� ";
$LANG_catstuff="���� ������ �����";
$LANG_email="������ ���������� ";
$LANG_siteurl="���� ������ ";
$LANG_bannerurl="���� ������� ";
$LANG_signsub="���� ��� ����� ������� �� ����";
$LANG_signres="����� �����";
$LANG_newsletter="������ ����� ������� ";
$LANG_coupon="��� �������";
$LANG_rejected="���� ����� ������ ������� ������� �";

// success messages..
$LANG_signup_thanks="����� ������� �� $exchangename !";
$LANG_signupinfo="�� ����� ����� ��� $exchangename ����� ! . ���� ����� ������ �� ���� ������� ���� ����� ��� ������ , �� ��� �� ����� ������� . ��� �� ����� ������ ����� ��� $email . ��� ��� ��� ��������� �� ���� ��� ����� ����� ��� ������ . ��� ����� ����� ������ ������� ��� <a href=\"index.php\">���� ����� ������</a> �� �� ��� . �� ���� ���� ������ ����� ����� ������ ����� , ������ ��������� , ����� ���� ����� , ����� ���� ������ , ��� .<p>";
$LANG_coupon_added="�� ����� ����� �� ���� <b>$newcredits</b> ���� .";
$LANG_signup_uploadmsg="������ � ����� ��� ����� ������ � ����� ����� �� ��� ����� ����� !";

// Conditions of Use page (/conditions.php)
$LANG_coutitle="���� ��������";
$LANG_header="���� �������";
$LANG_agree=" ������ ";
$LANG_disagree="��� �����";

// Top banners/accounts page (/top.php)
//top 10 banners:
$LANG_top10_title="���� $topnum ������";
$LANG_top10_exposure="�����";
$LANG_top10_banners="�������";
$LANG_top10_nobanners="�� ���� ������ ����� �� $exchangename ������ , ���� �� ���� ��� �� ������ ��� ���� ! ������ � �������� ���������� �� ���� �� ��� ������ , ��� ���� ������ ������� ������� !";

//Overall stats (/overall.php)
$LANG_overall_totusers="�������";
$LANG_overall_exposures="������";
$LANG_overall_banners="��������";
$LANG_overall_totclicks="������� ��� �������";
$LANG_overall_totsiteclicks="������� �� �������";
$LANG_overall_ratio="������� - ���� �������";


// common stuff...
$LANG_yes=" ��� ";
$LANG_no=" �� ";
$LANG_top="������";
$LANG_topics="��������";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>