<?
$file_rev="041305";
$file_lang="ar";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Error title and headers..
$LANG_error="���";
$LANG_error_header="���� ������� ������� ���� ������� �";
$LANG_back="������ �����";
$LANG_tryagain="����� ���� <a href=\"javascript:history.go(-1)\">������ �����</a> �������� ������ .";

// Login error stuff
$LANG_login_error="��� �� ��� �������� �� ���� ������ ! ����� ���� <a href=\"index.php\">������ �����</a> � ���� ����� !";
// Client link is different..so we have to repeat this.
$LANG_login_error_client=$LANG_login_error="��������� ���� ������� ��� ����� �� ��� ����� ����� , ���� <a href=\"../index.php\">������ �����</a> ������ ��������� !";

// mysql connect error (used in both login and processing)
$LANG_error_mysqlconnect="���� ������� ������ �������� ��� ��������� ������� . � �� ���� ������� ������� �";

// ADMIN: add account error: We only check the username
// because we expect the admin to know what he/she is doing...
$LANG_addacct_error="��� �������� ����� ������ , ����� <a href=\"javascript:history.go(-1)\">���� ����������</a> ������ ����� � �������� �� ���� .";


// ADMIN: add admin errors..
$LANG_adminconf_login_long="��� �������� ���� ������ ��� ����� . ( ��� �� ���� ��� �� 20 ���� ) .";
$LANG_adminconf_login_short="��� �������� ���� ������ ��� ����� . ( ��� �� ���� ��� ����� �� ����� ) .";
$LANG_adminconf_login_inuse="��� �������� $newlogin ����� �� ��� .";
$LANG_adminconf_pw_mismatch="����� ������ ��� ��������� !";
$LANG_adminconf_pw_short="���� ������ ��� ������ . ( ��� �� ����� �� 4 ����� ) .";
$LANG_adminconf_goback="����� <a href=\"javascript:history.go(-1)\">���� �����</a> �� ��� �������� .";
$LANG_adminconf_added="�� ����� ������ ����� !";

// ADMIN: Category admin errors...
$LANG_addcat_tooshort="��� ! , ��� ����� ��� �� �� ��� �� ����� .";
$LANG_addcat_toolong="��� ! , ��� ����� ��� �� �� ���� �� 50 ��� .";
$LANG_addcat_exists="��� ����� ���� ������ ����� ������ !";
$LANG_cats_nocats="�� ���� ����� ����� !  ��� �� ���� ���� ��� ���� ��� ����� .";
$LANG_delcat_default="�� ����� ��� ����� ��������� !";

// ADMIN/CLIENT: Change pw errors...
$LANG_pwconfirm_err_mismatch="����� ������ ��� ��������� !";
$LANG_pwconfirm_err_short="��� ! ���� ������ ��� �� ���� �� 4 ���� ��� ����� !";
$LANG_pwconfirm_err_intro="���� ������� ������� ����� ������ ����� ������� ";

// ADMIN/CLIENT: Upload/banner Errors...
$LANG_upload_blank="�� ��� ������ ������ ������ ������ . ���� ����� �� ���� ������ .";
$LANG_upload_not="���� ����� ������ ! . ���� ����� �� ���� �� ���� ";
$LANG_err_badimage="���� ��� ���� ������ ������ ��� ������ ��� ������ ������ . �� ���� ��� ���� ��� ���� ������ �� �� ����� ��� ���� ���� , ������ (<b>$bannerurl</b>) ��� ��� �� ��� ������ , ����� ���� ����� ���� ����� . ���� ������ �� ������ � �������� �� ���� . �� ������ ��� �� ��� ��� ������ ���� ����� ��� Geocities �� Angelfire ����� ����� ��� ����� ������ �� ���� ��� �� ���� �������� ��� �� ��� , ���� �������� �� ���� ��������� ������� ������ �� ��� .";
$LANG_err_badwidth="������� ���� �� ���� ������� . ��� ������� ������� $bannerwidth ���� .";
$LANG_err_badheight="������ ������� ��� ����� . ������ ������� ������� $bannerheight ���� .";
$LANG_err_filesize="��� ������� ������ ����� ������� . ��� �� ���� �� $max_filesize ���� .";

// Add admin error..
$LANG_adminconf_loginexist="��� �������� ����� �� ��� ��� ��� !";

// edit templates error..
$LANG_editcsstemplate_errornofile="����� ������ ��� ����� ! ���� ��� ���� ��� ��� ��� ����� �� ����� ������� ��� ��� ����� . ������ ����� , �� ���� ����� ������� . ���� ������ ����� � �������� �� ���� .";
$LANG_editcsstemplate_cannotwrite="���� ������� ��� ����� css.php . ���� �� ���� ������� ( 755 �� 777 ) �� ��� �� ���� �� ������ ��� ��� ����� . ���� ��� �������� ������ ������ ��� �������� .";

// Promo Manager errors..
$LANG_promo_noproduct="�� ��� ������ ��� ����� !";
$LANG_promo_badcode="�� ������ ����� �� �� ����� ������ ������ !";
$LANG_promo_noval="��� ����� ���� �� ��� \"������\" ������ �� ������� ��� ����� .";
$LANG_promo_nocreds="��� ����� ���� �� ��� \"������\" ������ �� ������� ��� ����� .";

// "COMMON"/PUBLIC SECTION ERRORS

// Lost Password error -- unable to locate account.
$LANG_lostpw_noacct="���� ������ ��� ���� ������ ������ <b>{email}</b> . ���� �������� �� ���� .";

// Signup Errors (/signupconfirm.php)
$LANG_err_nametooshort="��������� ���� ������� �� ��� <b>����� ������</b> ��� ������ . ( ��� �� ���� ���� �� ����� - ��� ����� <b>$_REQUEST[name]</b>)";
$LANG_err_nametoolong="��������� ���� ������� �� ��� <b>����� ������</b> ��� ������ . (  ��� �� ���� ��� �� 100 ���� -- ��� ����� <b>$_REQUEST[name]</b>)";
$LANG_err_loginshort="��������� ���� ������� �� ��� <b>��� ��������</b> ��� ������ . ( ��� �� ���� ��� �� 20 ����  - ��� ����� <b>$_REQUEST[login]</b>)";
$LANG_err_loginlong="��������� ���� ������� �� ��� <b>��� ��������</b> ��� ������ . ( ��� �� ���� ��� ����� 2 ����  - ��� ����� <b>$_REQUEST[login]</b>)";
$LANG_err_logininuse="��� �������� $_REQUEST[login] ������ �� ��� ��� ��� .";
$LANG_err_emailinuse="<b>����� ������ ���������� </b> ���� ������ , <b>$_REQUEST[email]</b> , ������ �� ��� . �� ���� ������� ��� ������ ����� �� ���� .";
$LANG_err_invalidurl="���� ������ ��� ����� . ���� ������ �� ����� ��� ����� ( ���� � index.html ��� ) �� ���� ����� �� ������� (http://www.somesite.com/ ) ! ��� ����� <b>$_REQUEST[targeturl]</b>";
$LANG_err_badimage="���� ������ ��� �� ������ ��� ������� . �� ���� ��� ���� ��� � ��� ������ , �� ��� ��� ���� ���� . ������ ������ (<b>$_REQUEST[bannerurl]</b>) .  ���� ������ �� ��� ������ � �������� �� ���� . ������ � ��� ���� ������ ��� ���� ����� ��� Geocities �� Angelfire , ����� ����� ��� ��� ������ �� ���� ������� �� ���� ����� �� ��� . ��� �������� �� ���� ���� ��� ������� .";
$LANG_err_badwidth="������ ��� ������ , ��� ���� ����� <b>$imagewidth</b> ���� . ����� ������� �� <b>$bannerwidth</b> ���� .";
$LANG_err_badheight="������ ������� ���� �� ���� ������� ��� <b>$imageheight</b> ���� . �������� ������� �� <b>$bannerheight</b> ���� .";
$LANG_err_email="����� ������ ���������� ����� ����� ��� ������ . ����� ������� �������� ���� �������� . ( ��� ����� <b>$_REQUEST[email]</b>) .";
$LANG_err_passmismatch="����� ������ ��� ��������� ! <b>$_REQUEST[pass]</b> ��� ������ �� <b>$_REQUEST[pass2]</b>";
$LANG_err_passshort="��������� ���� ������� �� ��� <b>���� ������</b> ��� ������ .  ( ��� �� �� ��� �� 4 ����� -- ��� ����� <b>$_REQUEST[pass]</b>).";
$LANG_err_nocoupon="����� ���� ������ ��� ����� �� �� ��� ������� ��� ���� !  ������� ������ ����� ������ .";

// Client coupon errors...
$LANG_coupon_wrongtype="��� ����� �� ������� �� ���� ������ ��� ����� ������� !";
$LANG_coupon_nocoup="������� ������� ��� ����� ! ������� ������ ����� ������ !";

// Promo code errors..
$LANG_coupon_clntwrongtype="��� ����� �� ������� ���� ��� �� ����� ������� !";
$LANG_coupon_noreuse="�� ���� ����� ������� ��� ������� !";
$LANG_coupon_userwrongtype="�� ����� ������� ��� ������� !  ��� ������� ������ ��� ��� ������� !";
$LANG_coupon_noreuseyet="�� ����� ������� ��� ������� ������ ! ���� ����� ������� ��� ������� ������ $date_placeholder .";

// Banner delete error
$LANG_bannerdel_error="�� ���� ��� ������� , ����� �� �� ������� �� �������� !  ��� �� ���� ���� ����� . ���� ��� ���� ��� �������� � ���� �� ���� �� ���� ������ !";

// e-mail change error
 $LANG_infoconfirm_invalid="����� ������ ���������� ����� ����� ��� ������ �� ��� ���� . ���� �������� �� ���� �� ������� �������� �������� .";

 // Password change errors
$LANG_err_nopassmatch="����� ������ ��� ��������� !";
$LANG_err_passtooshort="������ ��� ������ . ��� �� ���� ��� ����� 4 ����� .";
?>