<?
$file_rev="041305";
$file_lang="AR";
////////////////////////////////////////////////////////
//                 phpBannerExchange                  //
//                   by: Darkrose                     //
//              (darkrose@eschew.net)                 //
//                                                    //
// You can redistribute this software under the terms //
// of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of  //
// the License, or (at your option) any later         //
// version.                                           //
//                                                    //
// You should have received a copy of the GNU General //
// Public License along with this program; if not,    //
// write to the Free Software Foundation, Inc., 59    //
// Temple Place, Suite 330, Boston, MA 02111-1307 USA //
//                                                    //
//     Copyright 2004 by eschew.net Productions.      //
//   Please keep this copyright information intact.   //
////////////////////////////////////////////////////////

// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

//Install and upgrade will share some common variables..
$LANG_yes="���";
$LANG_no="��";
$LANG_title="����� ����� ������ phpBannerExchange";
$LANG_install_verbage="<b>������ �� �� ����� ����� ������ phpBannerExchange 2.0 !</b><p>��� ������� ����� ����� ���� ����� ����� ������ phpBannerExchange 2.0 , ����� ������� �� ���� ����� ������ � ���� ��� ����� �� ���� ������ ! . ��� ��� ������ , ����� �������� ����� <b>7</b> ����� ������ ������ �� ��� . ��� ����� �� ����� �� ��� ��� ���� �� ����� ����� ������ <a href=\"../docs/install.php#quickstart\">���� �������</a> .";
$LANG_install_version_found="�� ����� �� ���� ����� ���� ���� �������� ! <a href=\"install.php?install=3&page=2\">���� ����� �������</a> .";
$LANG_install_version_donno="���� ������ ��� ����� config.php , ���� ������ ��� ����� ���� ( ���� ��� ) . �� ��� ������ , ���� ������ ����� ������� �� ������ �";
$LANG_install_install="����� ����";
$LANG_install_instdesc="���� ��� ��� ������ �� �� ��� ��� ������ ������ phpBannerExchange ��� ����� �� ��� , �� ��� ��� ���� ������� ������ ���� ����� �� ��� �������� . <b>����� ! �</b> ��� ������� ����� ��� ����� �� ������ ������ ������ ������� phpBannerExchange �� ����� �������� !";
$LANG_install_upgrade="������� �� ������� 1.x ��� ������� 2.0";
$LANG_install_upgdesc="������ ��� ������ ������ ������� ������ �� ������� phpBannerExchange 1.2 ��� ������� ������ �� ������ phpBannerExchange .";

$LANG_install_rcupgrade="������� �� ������� 2.0 RCx ��� ������� 2.0";
$LANG_install_rcupgdesc="������ ��� ������ ��� ��� ���� ����� ���� �� phpBannerExchange 2.0 ���� ( ��� 2.0 RC1 ) , � ���� ������ ��� ������� ������ .";

$LANG_varedit_dirs="��� ����� ������ ��������� �������� ������� , ��� ��� �������� , ���� ������� , ������ ���������� , . ��� . ������ �� �������� ����� ������ <a href=\"../docs/install.php\">������� �������</a> , ��� ���� ��� ����� �� ������� ��� ������� ����� �� ���� �� �������� ���� ���� ���� ��� ������� , ���� �� ���� ����� ��������� ������� �� ���� ���� ��� �� ���� , � ���� ��� ������� .";

// headers
$LANG_varedit_dbhead="������� ����� ��������";
$LANG_varedit_pathing="�������� & ������� �������";
$LANG_varedit_bannerhead="��������";
$LANG_varedit_anticheathead="������� ��� �����";
$LANG_varedit_refncredits="��������� � ������";
$LANG_varedit_misc="������ ����";

$LANG_varedit_dbhost="���� ����� �������� ���� ���� ( localhost )";
$LANG_varedit_dblogin="��� ������ ����� �������� ";
$LANG_varedit_dbpass="���� ���� ����� ��������  ";
$LANG_varedit_dbname="��� ����� �������� ";
$LANG_varedit_baseurl="���� �������� ������� ���� http://www.yourdomain.com/exchange";
$LANG_varedit_baseurl_note=" ���� ������ ������� �� ������� ";
$LANG_varedit_exchangename="��� �������� ( ���� - ���� ����� ��������� )";
$LANG_varedit_sitename="��� ������ ( ������ ������� ���� - ��� ���� ) ";
$LANG_varedit_adminname="��� ������� ( ���� ���� ������� ������� ������� ) ";
$LANG_varedit_adminemail="������ ���������� ������ ( ��� ����� ��� ��� ������� ) ";
$LANG_varedit_width="��� ������� ������� �� ( ���� 468 ) ";
$LANG_varedit_height="������ ������� ������� �� ( ���� 60 ) ";
$LANG_varedit_pixels="���� ";
$LANG_starting_credits="����� ������ ( ��� ���� ��� ������� ����� ���� ������ ���� ������ �� ����� ) ";
$LANG_varedit_imgpos="���� ���� ���� �������� ������� ������� ";
$LANG_varedit_duration="����� ";
$LANG_varedit_duration_msg="����� ...";
$LANG_varedit_showtext="��� ���� �������� ��� ������ ( ��� ����� ��� ���� ���� ���� ����� �� ��� ����� ) ���� ( ���� �������� ��� �������� ) ";
$LANG_varedit_defrat="���� ������� ���������� ( �� ����� ����� ����� ��� ���� ��� ������� �� ���� ����� ����� ��� ������ ���� � ������ 3 ���� ���� ��� �������� 3 ���� ����� ��� ������� �� ����� ������� , ��� �� ����� ������ ����� ��� ������ ���� ����� ����� ��� �� ����� ����� ) ";
$LANG_varedit_showimage="��� ���� �������� ( ���� ����� ���� ����� ������� ���� ��������� ) ";
$LANG_varedit_imageurl="���� ���� �������� �� ��� ����� ������ ( ���� http://www.yourdomain.com/image.gif )";
$LANG_varedit_imageurl_msg=" ���� ������ ���� ���� ";
$LANG_varedit_sendemail="���� ����� ������� ���������� ��� ����� ��� ���� ";
$LANG_varedit_usepages="������� ����� ������� ";
$LANG_varedit_usemd5="������� ���� MD5 ������ ���� ������ ( ����� ��� ����� �������  ) ";
$LANG_varedit_topnum="���� X ���� ���� ���� �� ������ �������� ( ��� ���� ������� ) ";
$LANG_varedit_topnum_other=" ���� ";
$LANG_varedit_upload="������ ����� ���� ������ ��� ����� ";
$LANG_varedit_maxsize="����� ������ ������� ( �� ��� ����� ������ ������ �� ���� ����� ���� - 30000 ���� = 30 ���� ) ";
$LANG_varedit_uploadpath="���� ����� �������� ( ���� ���� ����� �� ������� ) �� ���  /home/username/public_html/exchange/upload  ����� ��� ����� ���� �� ��� �� ��� ������ ������ ���� �������� ";
$LANG_varedit_upurl="������ ������ ��� ���� ��� �������� ��� http://www.yourdomain.com/exchange/upload ( ���� ���� ����� �� ������� )  ����� ��� ����� ���� �� ��� �� ��� ������ ������ ���� �������� ";
$LANG_varedit_referral="������� ���� ��������� ( ��� ����� ���� �� ���� ������� ���� �� ������ ��� ����� ��� ���� ������ ��� ���� ����� ������� ����� �� ������� ) ";
$LANG_varedit_bounty="���� ������� ( ��� ������ ���� ������ ����� �� �� ����� ) ";
$LANG_varedit_usegzhandler="������� GZip ���� ������� ( ���� ������ �� ���� ������� � ���� ���� ������ ) ";
$LANG_varedit_usedbrand="������� mySQL4 rand(): ( ��� ������ ���� �� ���� exchange ����� �������� �������� � ���� ����� ����� mySQL 4.x �� ���� ) ";
$LANG_varedit_usedbrand_warn="��� mySQL 4+ !";
$LANG_varedit_maxbanners="����� ������ �������� ���� ���� ����� ���������� ��� ���� ";
$LANG_varedit_basepath="������ ������� ( ��� ������ ) ��� �� ��� ���� �������� , �� �� ��� ��� ���� �� ���� ��� ( /home/www/exchange)";
$LANG_varedit_sellcredits="���� ��� ������ ( ��� ������ ���� ���� ������� �� �� ��� ������� , ��� ��� ���� ���� ���� ���� ������ , ���� ��������� ������ ) ";
$LANG_varedit_anticheat="����� ��� ���� ";
$LANG_varedit_cookies="�������";
$LANG_varedit_db="����� ��������";
$LANG_varedit_none="����";
$LANG_varedit_reqapproval="���� �������� ��� ������� ";
$LANG_varedit_usegz="������� ��� gZip/Zend";
$LANG_varedit_userand="������� ���� mySQL4 rand()";
$LANG_varedit_userandwarn="����� ����� mySQL 4 �� ���� ";
$LANG_varedit_logclicks="����� ������� ( ����� ��� � ����� � IP �� ���� ��� ������� �� ����� �������� , � ��� ���� ���� �� ��� ����� �������� ) ";
$LANG_left=" ���� ";
$LANG_right=" ���� ";
$LANG_top=" ���� ";
$LANG_bottom=" ���� ";
$LANG_varedit_reqbanapproval="������ ������� ��� ������� � ( ��� ����� ����� ����� �� ������� ) ";
$LANG_varedit_dateformat="����� ������� ";

$LANG_varedit_submit=" ����� ";
$LANG_varedit_reset=" ����� ����� ";

$LANG_fput_error_config="���� ����� ��� ��������� . ���� ��� ����� ���� ��� �� ������� �� �� ����� �� ���� ������� ������ ��������� . ���� �� ��� ��� ������ ������� 777 ����� . �� ���� ����� ����� ���� �������� ��� ��� ������� .";
$LANG_fput_chmod="��� ������� ��� ����� ������� ������ � ���� ��� . � ��� ���� ��� �� ����� ��� ����� �� ������ ������ ( ���� ������ base_path ) , �� �� ������ �� ����� ��� �������� ������ ������� . ���� ��� ������� ����� ������ . �� ���� ����� ����� ���� �������� ��� ��� ������� .";
$LANG_fput_success="�� ����� ��� config ����� ! . ��� ���� ���� �������� ��� ������ ������� .";
$LANG_db_problem="���� ����� �� ������� ������ �������� ������ �� ����� config . ���� ���� ����� �������� <b>$dbname</b> � ������ �� ���� ������ .";
$LANG_db_noconnect="���� ����� �� ������� ������ �������� .! ���� ������ �� ��� ����� ������ ������ ��� ����� �������� ������� �� ��� ��������� � �� ���� ������ ����� .";

$LANG_tables_created="�� ����� ���� ������� ����� !";
$LANG_upgrade_db="���� ����� ������� , ���� �������� ...";
$LANG_upgrade_done="�� ����� ������� ����� !";
$LANG_admin_add_instructions="����� ���� ������� . ������ phpBannerExchange ���� ���� �������� . ����� ����� ������ ������ ��� ���� ������ ��� �������� �� ������� .";
$LANG_admin_login="��� �������� ";
$LANG_admin_pass="���� ������ ";
$LANG_again="������ ";
$LANG_password_mismatch="����� ������ ��� ��������� , ����� ���� ����� � ��� ��� ���� .";

$LANG_install_complete="�� �������� �� ����� ������ phpBannerExchange 2.0 ����� ���� ! ����� ���� ������ ��� <a href=\"../admin/\">���� ������ ��������</a> �������� ��� �������� � ���� ������ .<p><b>����� ���� � ��� ��� ������ INSTALL ���� ���� �� ���� ! ( �� ����� ���� ��� �� �� ��� ����� ��� ����� ) ! ! .</b>";

$LANG_continue="���� ����� ��������";

$LANG_install_oldvarupgrade="<b>������ �</b> <b>���</b> ������� ��� ����� ������ ����� ��� ��� ���� �������� �� ����� phpBannerExchange 1.x ������ ����� �������� ! ������ ������� ����� ������� ������� �������� ������� !";
$LANG_install_oldverupg="������ ������� ���� ���� ������ ������� ������� . ��� ������� �� ������ ��� ����� � ��� ����� ���� �������� �� ����� �������� ������� . �������� , ���� ��� ����� �������� ������� �� ������ �";
?>