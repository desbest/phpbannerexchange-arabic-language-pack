<? include ("../css.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="rtl">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1256">
<meta http-equiv="Content-Language" content="ar-sy">
<title>����� �������� ������� phpBannerExchange 2.0</title>
<link rel="stylesheet" href="../template/css/<? echo "$css"; ?>" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0"
  marginheight="0" >
<div id="content">
<div class="main">
<table border="0" cellpadding="1" width="650" cellspacing="0">
<tr>
<td>
<table cellpadding="5" border="1" width="100%" cellspacing="0">
<tr>
<td colspan="2" class="tablehead"><center><div class="head">����� �������� ������� phpBannerExchange 2.0</center></div></td>
</tr>
<td class="tablebody" colspan="2">
<div class="mainbody">
<table border="0" cellpadding="1" cellspacing="1" style="border-collapse: collapse"  width="90%">
  <tr>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="90%" >
<tr>
<div class="lefthead"><a NAME="introduction">�������</a></div>
<p>
phpBannerExchange 2.0 ������ ��� ������� ������ ��������� ������ �������� , ��� ���� PHP4 � mySQL ���� ������ Apache ���� ��� Linux . ��� ��� ����� ���� ��� ������ ���� ��� Microsoft SQL � Internet Information Server (IIS) , ������ �� ��� ������� ��� ��� �������� .<p>
�� ����� ��� ������ �������� ��� ��� ������� ������� �� �������� � ������ ������ ������ �������� ������� �������� ������ ����� �������� .  ��� ������ ����� ��� �� ��� ������ �������� ����� � ���� <a href="install.php">������� �������</a> .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>

<div class="lefthead"><a NAME="stats">����� ������ � ������ ����������</a></div>
<p>
���� ������ �������� ����� �� ������ ����� �������� �������� ������ , ����� / ��� / ����� / ������� , ��� �������� , ����� �������� , ��� .  ������ ��� ���� ������ , ����� ��� ����� ������� ��� ������ ������ �

http://www.<i>yourdomain.com</i>/<i>exchange_directory</i>/admin/<p>

������ <i>yourdomain.com</i> �������� ����� �� ( ���� � eschew.net ) � <i>exchange_directory</i> ������� ����� �������� ��� ����� ( ���� � bannerex ) . ����� �� ����� ������ . �� ������ ������ �������� ��� �������� � ���� ������ ���� ����� ��� ����� �������� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead">����� ����������</div><p>
���� �� ����� ���������� ������� ������ . ��� ���� �� ���� ������� �������� , ������� ���� ������ ���� ����� �� ��� . �� ������ ������ ��� ���������� �
<table class="tablebody" width="100%">
<tr>
<td width=180 valign="top"><b>������ ���� �</b></td><td>��� ������� ����� ������� ������ ��� ������� .</td>
</tr>
<tr>
<td width=180 valign="top"><b>����� ������ �</b></td><td>����� ��� ��������� ����� �������� .</td>
</tr>
<tr>
<td width=180 valign="top"><b>������ ������� �</b></td><td>����� ������ ������� ������� ��������� .</td>
</tr>
<tr>
<td width=180 valign="top"><b>����� �������� �</b></td><td>��� �������� ������� . ��� ������ �� ������ � ������ �������� .</td>
</tr>
<tr>
<td width=180 valign="top"><b>������� �������� �</b></td><td>��� ������� �������� �� �� ��� ��������� ������� ������ ������ .</td>
</tr>
<tr>
<td width=180 valign="top"><b>����� ������� ��� ������� �</b></td><td>��� ������ ���� �� ���� ����� ��� ������� ��� ���� ����� .</td>
</tr>
<tr>
<td width=180 valign="top"><b>����� ������� �� ������� �</b></td><td>��� ������ ���� �� ���� ����� ��� ������� �� ���� ����� .</td>
</tr>
<tr>
<td width=180 valign="top"><b>���� ������� ������ �</b></td><td>���� ������� ��� ������ �������� ��� ��� ������� �� ���� ����� .</td>
</tr>
</table>

<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="validate">������� ��������</a></div>
<p>
����� �� ���� ������� ����� �� ��� �������� �� ������� ������ �� �������� ��� �������� . ��� ����� ������ �� ������ ������ � ������� ��� �������� ��� ����� ������ . ���� , �� ��� ������� �� ���� ����� ���� ����� ������� �������&#0153; � �� ������ ��� ���� ����� ��� ���� ������ , ��� ���� ������ ��� �������� ���� ����� ���� ������� �� �������� ��� �� ������ �� ������ ����� ��������� .<p>
��� ��� ������ ��� ����� "������� ��������" ����� ���� ����� ���� �� ������� ���� , ��� ����� ���� ������ �� ����� ������� �� ����� .<p>
�������� ���� ����� �������� ����� �� ���� "������� ��������" . ������ ���� , ������ ���� ��� ��� ������ , ���� ���� ��� ���� ������ ��� ����� ����� �� �������� . �� ������ ��� ��� �������� ������� �<p>

<table class="tablebody" width="100%">
<tr>
<td width=140 valign="top"><b>����� ������ �</b></td><td>����� ������� ������ ����� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>��� �������� �</b></td><td>����� ���� ������ ����� ��������� ��� ����� ������ .</td>
</tr>
<tr>
<td width=140 valign="top"><b>���� ������ �</b></td><td>��� ����� ���� ���� ������ ����� . ��� ��� ������ ���� ������� MD5 . ����� ���� ������ ���� �� ������� . ����� ��� ������ ������ �� ����� ���� ������ ������� , ��� ��� ��� ������ ���� ������� MD5 .</td>
</tr>
<tr>
<td width=140 valign="top"><b>������ ���������� �</b></td><td>����� ������ ���������� ���� ����� ����� ��� ������� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>����� �</b></td><td>������ ����� ����� ����� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>������ �</b></td><td>��� ���� ���� ����� ����� ���� ��� ����� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>������ �</b></td><td>��� ������ ������� �� ���� ����� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>������ ��� ������ �</b></td><td>��� ������� ��� ����� ����� �� ������� ������ .</td>
</tr>
<tr>
<td width=140 valign="top"><b>������ �� ������ �</b></td><td>��� ������� ��� �������� �� ���� ����� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>��� ������ ������� �</b></td><td>��� ������ ���� �������� ������ ���� ��� ������ ����� �� ���� ��� � ������ �� ������ . �������� ��� ������ , ����� ������ ��� HTML ���� �� ������ ����� �� ����� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>���� ������ �</b></td><td>������ ���� ���� �� ����� ���� �������� "���" .</td>
</tr>
<tr>
<td width=140 valign="top"><b>������ ��������� �</b></td><td>�� ���� �� ���� ��� ���� ��� ��� ��� ���� ������ ���� ���� ������ ��� ���� ����� ���� �������� . ���� ��� ��� �������� ���� ��������� ������� �� ������� "��� ��������" �� ������� ������ ( �������� ) , �� ���� ������� �������� ����� ��� ����� ����� ��� ����� . ��� ��� ���� ��� ��� ������� ���������� ��� ���� ��� , ������� ��� ����� ��� ������ ��������� .</td>
</tr>
<tr>
<td width=140 valign="top"><b>����� ����� ������� �</b></td><td>��� ������ ������ �������� �� ������� �������� / ������� ��������� , ����� ������ ���� ������ �� ���� <a href="admin.php#mailer">���� ���������</a> .</td>
</tr>
<tr>
<td width=140 valign="top"><b>�������� �</b></td><td>�� �������� ���� �������� ����� �� �������� . ������� �������� ��������� , ���� <b>��� / ����� ��������</b> . �������� ������� �� ��� ������ ���� ����� .
</td>
</tr>
<table>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="addacct">����� ���� ����</a></div><p>
��� ������ ����� �� ����� ���� ���� ��� �������� . � �� ��� �������� ������� �� ����� <a href="admin.php#validate">������� ��������</a> .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="listacct">����� ����� ��������</a></div><p>
��� ������ ���� ��� ����� ����� �������� ������ ������ ����� . ����� ��� ��� ������ ����� �� ����� ������ ������ . � ���� ������ ��� �������� ������� �� ������ <a href="admin.php#validate">������� ��������</a> .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="defbanner">������� ����������</a></div><p>
������� ���������� ���� ��� ��� ���� ������ ���� ����� ����� ���� �� ������� . ��� ��� �� ���� ������� ���������� ������ ��� ���� ����� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="mailer">���� ���������</a></div><p>
<p>
�� ��� ����� ������ ���� ������� �� �������� , ��� ����� ����� ������� ��������� ������� �<p>
<table class="tablebody" width="100%">
<tr>
<td valign="top">
<b>%username%</b></td><td> ����� ������ ����� .</td>
</tr>
<tr>
<td valign="top"><b>%login%</b></td><td> ��� �������� .</td>
</tr>
<tr><td valign="top"><b>%email%</b></td><td> ������ ���������� .</td>
</tr>
<tr><td valign="top"><b>%statstable%</b></td><td> ���� �������� ��� ���� ����� ����� ��� ��������� .</td>
</tr>
<tr>
</table>
<p>
���� ������� ������� ������ Unix �������� ��� ���� (\n) ����� ���� ���� , ����� ������� HTML . ���� ��������� ���� ������ �� HTML �������� � ���� ������ ������� ������ mime ������ ����� �� ����� ����� HTML ��� ���� ������ .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="cats">����� �������</a></div><p>
����� ��� ����� ������� , ����� , ��� , ����� ������� . ����� ������� ����� �� ����� ����� .<p>
���� ��� ��� ����� ������� �������� ���� ����� ���� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="admin">����� / ��� �����</a></div><p>
��� ����� ����� �� ��� ��������� . ����� ����� ������� ���� ������� ������ ����� , ������ ��� ( �� �� ) .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="pw">����� ���� ������</a></div><p>
�� ��� ����� ����� ���� ������ ������ �� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="dbtools">����� ����� ��������</a></div><p>
����� �������� ����� ������ ��� ������� � ������� ���� ����� . ���� ���� ��� ������� �������� ���� ����� ����� ����� �������� . ������ ��� ������� mysqldump ����� ����� �� ������ <b>admin/db</b> �� ����� �� ������� �������� �� ��� ����� .<p>
<b>����� � ��� ����� ������ ����� ���� ������ ����� .HTACCESS !</b><p>
������ ����� , ��� �� ��� ������ ���� ������� � ������� ���� ��� . ���� ����� ����� ������ ������ ��� ��� ������� .<p>
����� ��� ��� ����� ��������� �� ����� , � ��� ������ ��� ��� ����� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="template">����� �������</a></div><p>
������� phpBannerExchange 2.0 RC3 � ������ ����� ����� ��� ��������� ����� �� ����� ������� ��� ���� . ���� ������� ����� ��� ������ <b>{item}</b> ����� ��� HTML . ����� ���� ������� ��� ����� ���� ��������� .<p>
������ �������� ������� �<p>
<table class="tablebody" width="100%">
<tr>
<td width=130 valign="top"><b>{title}</b></td><td>��� �������� � ���� ����� ����� ������� . ���� � "���� ����� ��������� - ����� ������" ��� .</td>
</tr>
<tr>
<td width=100 valign="top"><b>{shortitle}</b></td><td>�� "��� ������" ���� � "���� ������ ��������" ��� .</td>
</tr>
<tr>
<td width=100 valign="top"><b>{menu}</b></td><td>������ ��� ������� ��� ���� .</td>
</tr>
<tr>
<td width=100 valign="top"><b>{msg}</b></td><td>����� ������� , ��� � " �� ����� ������� ����� ! " ��� .</td>
</tr>
<tr>
<td width=100 valign="top"><b>{footer}</b></td><td>����� ��� ��� ������� .</td></tr></table>
<p>
���� ��� ���� �� ��� ������ ���� ����� ��� ����� �� ���� .<p>

���� ����� ������ �� ����� HTML ��� ������� . �� ����� ���� ��� ������ ������ ( ��� <b>while()</b> �������� ) ��� ���� ���� ����� ���� �� ������� PHP .

<p>�� ���� �������� ������ ����� ����� ����� ������ ����� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="variables">���� ���������</a></div><p>
��� ������� ����� �� ����� ������ ��������� �� ����� config.php �� ���� . ���� � ��� ���� ����� ���� ������ �� �� ��� , ����� ����� ��� ������ �� ��� .<p>
��� ����� �������� �

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="tablebody" width=150 valign="top">���� ����� �������� �</b></td><td class="tablebody" valign="top" dir="ltr">$dbhost</td><td class="tablebody">��� ������ ����� ������� . ������ ���� <i>localhost</i>.</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$dblogin</td><td class="tablebody">��� �������� ������� ������ �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ������ �</td><td class="tablebody" valign="top" dir="ltr">$dbpass</td><td class="tablebody">���� ������ ������� ������ �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ����� �������� �</td><td class="tablebody" valign="top" dir="ltr">$dbname</td><td class="tablebody">��� ����� ������� ���� ���� ��������� ������ ������ �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$baseurl</td><td class="tablebody">���� �������� �������� ��� �������� . ���� , ��� ��� ������ �������� ��� ������ "exchange" ���� ������ ��� <i>http://www.yourdomain.com/exchange</i> . ���� ���� ����� �� ������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$basepath</td><td class="tablebody">������ ( <b>� ���</b> ������ ) �� ���� �������� �� ����� . ��� ��� ��� ������ ���� ������ ������ ����� ������� �� ������� . �� �� ��� ��� �������� , ���� ������ ������ �����  ( ���� � <i>/home/www/exchange</i> ) . ���� ���� ����� ��� ��� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$exchangename</td><td class="tablebody" valign="top">��� �������� ���� ������ ������� � �� ���� ������ � ����� ������ ���������� ��� .  ���� � ���� ����� ��������� ) .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �</td><td class="tablebody" dir="ltr">$sitename</td><td class="tablebody">��� ������ ������� , ������ �� ������ � ���� ������� ( ���� � ��� ���� ) .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �</td><td class="tablebody" valign="top" dir="ltr">$adminname</td><td class="tablebody">���� �� ��� ������ . ������ ���� ����� ���� �� ���� , ����� ����� ������ ������ ���������� ������� ����� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ������ �<td class="tablebody" valign="top" dir="ltr">$ownermail</td><td class="tablebody">����� ������ ���������� ���� ���� �������� ����� ������� � ������� �� �������� ����� ��� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������� �</td><td class="tablebody" valign="top" dir="ltr">$bannerwidth</td><td class="tablebody">��� ������� ������� �� ������� , �������� �������� ���� ���� 468 ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ������� </td><td class="tablebody" valign="top" dir="ltr">$bannerheight</td><td class="tablebody">������ ������� ������� �� ������� , �������� �������� ���� ������� 60 ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ������� ���������� �</td><td class="tablebody" valign="top" dir="ltr">$steexp, $banexp</td><td class="tablebody">����� ���� ������� ���������� ������� ���� ���� ���� ������� �� ���� ����� . �� ����� ����� ��� ����� �� ��� ��� ���� �������� �� ���� ����� ����� ��� ����� ����� �� ���� ��� ���� ���� . ���� � ������ "3" ���� ��� ����� ����� ���� ����� ����� ���� ���� �������� �� ����� . ��� ����� ������ ��� ������ ��� ������ ���� ����� �� ���� ����� ���� ��� ������ ���� ����� . ���� � ������ "3" ���� ��� 3 ���� ��� ��� . �� ������ ��� ��� ����� ������� ��������� �<p>
<b>1</b> ��� ���� �� ���� ����� ����� � <b>1</b> ��� ������ ����� � <b>1 ����� 1</b><br>
<b>2</b> ��� ���� �� ���� ����� ����� � <b>1</b> ��� ������ ����� � <b>1 ����� 2</b><br>
<b>3</b> ��� ���� �� ���� ����� ����� � <b>2</b> ��� ������ ����� � <b>1 ����� 1.5</b></td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$showimage</td><td class="tablebody">��� ����� ����� ��� �� ��� ���� ���� ���� ����� �������� ����� ������� . ���� ��� ���� ����� 60X60 ������� ��� 468X60 ���� . ����� ���� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$imagepos</td><td class="tablebody">��� ���� ��� ���� �������� �� ������ ������ , ����� ��� ����� �������� ������� ������� , ���� , ���� , ���� , �� ���� ������� . ��� ����� ���� �� ���� ��� ������ ������ ���� ����� (468X60) ���� , ��� ��� ��� ������� ������� ��� 468 ���� . </td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$imageurl</td><td class="tablebody">��� ���� ��� ���� �������� �� ������ ������ , ���� ��� ������ ������ ������ (���� � <i>http://www.yourdomain.com/image.gif</i>) . � ��� ����� ��� ����� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$showtext, $exchangetext</td><td class="tablebody">��� ��� ���� ���� ���� ���� ��� �������� ���� ������� , �� ��� ������ ��� "���" , �� ���� ���� ���� ���� ���� ������ �� ��� ���� . ( ���� � ���� �� ����� ������ �� "��� �������� " ) �� �� �� ��� ������ .
</td>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� ��� ������� �</td><td class="tablebody" valign="top" dir="ltr">$reqbanapproval</td><td class="tablebody">��� ������ ����� �� ������ �������� � �������� ����� ��� �� ��� ������� �� ������� . ����� ������ ��� <b>��</b> ���� ������ ��� �������� ��� ������ � ���� ������ ��� . ��� ����� ������ ��� <b>���</b> ����� ��� ������ �� ����� ������� �������� ���� ��� ����� ������ �� ����� �� ��� ������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$allow_upload</td><td class="tablebody">��� ������ ����� �� ����� ���� ������ ������ ������� ��� . ����� ��� ������ ��� <b>��</b> ���� ��� ��� ����� ������� ����� ����� ��� ����� . � ������� ���� ��� ���� �� ������� ���� ������� � ������� �������� �� ��� ��� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ ����� �</td> <td class="tablebody" valign="top" dir="ltr">$max_filesize</td><td class="tablebody">����� ����� ������ ���� ���� ������� ������� ������ "�������" . ���� � ( 30 ���� ��� ������� "30000" ) . ����� ��� ��� ����� ���� �� ��� �� ��� ������ ���� ����� ��� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� ��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$upload_path</td><td class="tablebody">���� ���� ������� �� *nix/Windows ���� ���� �� ��� <i>/home/username/public_html/exchange/upload</i> �� �� ���� . "���� ���� ����� ��� ������" ��� ������ ��� �� ���� ��� ���� ������� � ������� ( ��� ���� ������� ������� 777 ) . ���� ����� ����� �� �� ��� ����� �� ���� html . ����� ��� ��� ����� ���� �� �� ��� ������ ���� "����� ��� ��������" .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$banner_dir_url</td><td class="tablebody">��� ����� ���� "������" ��� ���� ������� ������ ������ �� ������ ������ . ���� <i>http://www.yourdomain.com/exchange/upload</i> "���� ���� ����� �� �������" ����� ��� ��� ����� ���� �� �� ��� ������ ���� "����� ��� ��������" .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� ������ �</td><td class="tablebody" valign="top" dir="ltr">$maxbanners</td><td class="tablebody">��� ����� ����� ����� ������ �������� ���� ���� ����� �������� ��� ����� . ��� ��� ���� ����� ������ �������� ��� ��� ����� �� �������� , �� ������ ��� "0" (zero) . ��� ������ ���� �� ��� ������� ( ��� �������� �� ��� ���� ��� ���� ������ ) .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ ���� �</td><td class="tablebody" valign="top" dir="ltr">$anticheat</td><td class="tablebody">����� ����� ��� ���� ���� ���� ��������� . �������� ������� <b>����� ��������</b> , <b>�������</b> , � <b>����</b>. <p>
������ <b>�������</b> ����� ��� ������� ������ �� ���� �������� ���� ������ ������ ����� . ����� ��� ��� ������ ����� ������� ��� ������ ����� ������ ��� ���� ����� . ( ����� ����� ��� �� ������ ������ ) .
<p>
������ <b>����� ��������</b> ����� ����� ������� � ��� ��� ��� ����� ��������� �� ����� �������� , ��� ��� ������� ������� , � �� ���� ��� , � ��� ��� ��� ��� ������� ���� ���� ���� ������ �� ��� ����� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ �</td><td class="tablebody" valign="top" dir="ltr">$expiretime</td><td class="tablebody">��� ��� ����� ����� �������� ���� ��������� ��������� ���� ���� . � �� ������ ������ ���� ����� ��� ������ ������ ��� ��� ����� ������ ���� �������� . ���� ������ ��� ����� , ��� <b> 20 �� 30  ����� </b> .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ������� �</td><td class="tablebody" valign="top" dir="ltr">$referral_program</td><td class="tablebody">��� ������ ����� �� ��� ������� ���� ������ ��� ����� ��� ���� ������ ����� ��� ����� �������� �������� �� ���� ����� , ���� � (<i>http://www.somesite.com/exchange/index.php</i>) � ���� ���� �� ���� ��� ��� ���� ���� ����� . ���� ����� ��������� �� ������� ��� ����� �� ���� ��� ���� , ��� ������ ������ ��� ��� ����� ������ ������ � �������� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ������� �</td><td class="tablebody" valign="top" dir="ltr">$referral_bounty</td><td class="tablebody">��� ������ ������ ��� ������ ���� ���� ����� ����� ����� �� ����� ������ ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �� �</td><td class="tablebody" valign="top" dir="ltr">$startcredits</td><td class="tablebody">��� ���� ���� ��� �� ������ �������� ����� ������ ��� ������ , ���� ����� ���� ���� . ����� ����� ��� ������ ��� ���� ���� � ���� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �</td><td class="tablebody" valign="top" dir="ltr">$sellcredits</td><td class="tablebody">����� ���� ��� ������ . ��� ������ ���� ���� ������� ��� �� ���� ������� , ��� �� ���� ��� ���� ������ . ���� ��� ����� ������� ����� �� ��������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ���� X  ���� �</td><td class="tablebody" valign="top" dir="ltr">$topnum</td><td class="tablebody">����� ��� "���� ��������" �� ��� ������ , ���� ����� �� ���� ����� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ �</td><td class="tablebody" valign="top" dir="ltr">$sendemail</td><td class="tablebody">��� ���� ����� ����� ������� ���������� ��� ����� ����� ���� ��� ������ "���" .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������� ���� MD5 ������ ����� ������ �</td><td class="tablebody" valign="top" dir="ltr">$usemd5</td><td class="tablebody">������ ����� , ����� ����� ����� ������ �������� ���� MD5 , ��� ��� ����� ���� ������ ��� "32" ���� . ���� �� ���� ������ ����� ��� ����� ������� ( ������ ����� ���� ) ��� ������� ��� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������� ��� gZip/Zend �</td><td class="tablebody" valign="top" dir="ltr">$use_gzhandler</td><td class="tablebody">��� ��� ���� ���� ������� � ����� ������ ���� gZip , �� ������ ��� ������ . ��� ������ ���� �� ������� ���� ������� , ����� ���� ������ ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ �������  �</td><td class="tablebody" valign="top" dir="ltr">$log_clicks</td><td class="tablebody">����� ��� & ����� � ����� IP �� ���� ������� �� ����� �������� . ���� ��� ��� ��� ��� ������� ���� ���� , ������ ����� ����� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������� ���� mySQL4 rand() �</td><td class="tablebody" valign="top" dir="ltr">$use_dbrand</td><td class="tablebody">����� ����� mySQL rand() �� ��������� ����� view.php . ��� ������ ���� ���� ���� �� ���� ���� �������� �� ����� ������ ��������� , ����� ����� �� ���� ���� ����� mySQL 4.x �� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� �������  �</td><td class="tablebody" valign="top" dir="ltr">$date_format</td><td class="tablebody">����� ��� ����� ������� �������� �� �������� .  ( mm/dd/yyyy �� dd/mm/yyyy ) .</td>
</tr>
<table>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="cssedit">���� ���� ������� ��������� Style Sheet</a></div><p>
���� ���� ������� ��������� Style Sheet ����� �� ����� �� ����� ��� �������� . ��� �� ����� ��� ������� ������� �� �������� .<p>

������ ����� ����� , ����� ������ ����� ��� ������ <b>template/css</b> ����� ����� ��� ������� Style sheets ������� phpBannerExchange 2.0 �� ���� ����� (<a href="http://www.eschew.net/scripts/">http://www.eschew.net/scripts/</a>). ������ ���� ������� , ���� �� �� ������� CSS �� ������ <b>template/css</b> ����� ������ � ������� ( ������� 777 ) . ����� , ��� ��� ������ ��� ���� , ����� ������ ��� , ��� ������ ������� �� ������� ������� .<p>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="faq">����� ��������� / ������� ��������</a></div><p>
��� ������ ����� �� ����� ������� �������� �� �� ���� ���������� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="dbtools">���� ������ ��������</a></div><p>
��� ��� ���� ������� �������� ���� ���� ��������� ��� �� ����� ��� ����� , ���� ������ ������ �� ���� ������ ��� �������� � ������ �� ����� ���������� . ��� ������ ���� ������� �������� ���� ������ ����� �� �������� ������ , � ��� ���� ���� �� ������ ����� ��� , �� �� ��� ������ �� ��� ����� . ��� ����� �������� ������ �� ����� ���� ��� ����� " ������� �������� " ��� ����� ������ �� ������ ��� �� ���� ����� ������� �� ����� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="cou">����� ������ � ��������</a></div><p>
���� � ������ ������� ��� ������ �� ������� �� �������� ��������� �� �� ������� ��� ���� ������ �� ��� ������� , ����� ��� ��� ��� ��� �������

���� �������� , � ���� ��� ����� , ������ ��� ��� ��� ��� �������� . ���� ���� ���� ������� ��� ��������� ��� �� , � ��� ����� ������� �� ���� .<p>

�������� ������ ����� ������� ���� ���� ������ �� ������ ������� . ���� � ��� ��� ���� ������ ������� ������� ������� , ��� ������ ������� �������� �� ���� ������� ����� ��� ���� . ��� ��� �������� ���� ��� .<p>

�������� �� ������� phpBannerExchange 2.0 RC2 � �� ��� , ��� ����� �������� �� ����� �������� .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead"><a NAME="promo">���� ������� �������</a></div><p>
���� ��������� ����� �� ����� ����� ���� ��� ������ ������� , ������� ������� �� ����� , �� ��� ��� ��� ���� �� ����� ������ ����� ������� . ������� ������ ����� ������ CaSe SeNsItIvE � ���� ������ �� ������� ( �� �������� �� !@#$%, ��� ) . ����� ��� �� ��� ��� ������ ����� �� ������ �������� ���� ������ ��� ����� . ���� ��� ������ �

<table class="tablebody" width="100%">
<tr>
<td valign="top" width="130">
<b>��� ������� �</b></td><td>��� ������� . ������ ����� �� ���� �� �� �� ��� .</td>
</tr>
<tr>
<td valign="top">
<b>����� �</b></td><td>��� ������� ���� ������ ����� ������ ��� ���� ��� ������� .</td>
</tr>
<tr>
<td valign="top">
<b>����� � </b></td><td>���� ���� ����� ����� ��������� �<p>
<b>���� ������ �</b> ������� �� ��� ����� ���� ��� �� ������ . ����� ��������� ������ ������� ��� ����� ���� ���� , ���� ����� �������� , ��� . ��� ����� �� ������� �� ���� ���� ����� ����� ����� .<p>
<b>XX% �� ����� �</b> ������ ��� ������� ������� ��� ��� �� ����� ������� .<p>
<b>��� ��� �</b> ������ ��� ��� �� ����� ������� ���� ���� ���� ��� ��� ����� ����� .</td>
</tr>
<tr>
<td valign="top">
<b>������ �</b></td><td>��� ����� ������ ����� ������ �� ����� ������� . ���� ��� <b>���� ������</b> , � �� ���� �� �� . ���� ��� <b>XX% �� �����</b> . ������ ���� ������ ������� . ��� ���� ��������� , ( ���� , ����� "50" �� ��� ����� ����� 50% �� ������� ) . ���� ��� <b>��� ���</b> , ������ <b>���</b> ����� .</td>
</tr>
<tr>
<td valign="top">
<b>������ �</b></td><td>��� ������ �������� ����� . ��� ����� ������ �� ���� ����� ������� .</td>
</tr>
<tr>
<td valign="top">
<b>���� ����� ����� ������� ��� ������� �</b></td><td>����� ��� ������ ���� ������� ����� ������� ��� ������� ���� ���� ��� ������ .</td>
</tr>
<tr>
<td valign="top">
<b>����� ������� ��� ��� ����� �</b></td><td>����� �� ��� ������ ����� ����� ������� ������� �� ����� . ��� ���� ���� ���� ��� �� ��� ������� <b>���� ������</b> . ����� ��� ���� ��� ��� �� ������ �� ��� . ���� , ��� ��� ���� <b>����� ������� ��� �������</b> ��� ����� , ���� ����� ��� ����� . ����� ��� ������ ��� "0" ���� ����� ����� ��������� ���� ���� .</td>
</tr>
<tr>
<td valign="top">
<b>��� ������� �������� �</b></td><td>����� ��� ������� ����� ���� ��� ������� ������� . �������� ������� �� <b>������� ����� ���</b> � <b>���� �������</b> .</td>
</tr>
</table>
<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="update">���� ���������</a></div><p>
���� ��������� ��� ����� ���� ������ �� ������� �������� �� ������ ������� . ��� �������� ��� ����� �������� , � ����� ������� ���� ���� . ���� ����� ������� ������ ������� �������� �� ����� ������ ���� �������� ������ , � ������ ����� �� �������� . ������� �� ���� <b>��������</b> ����� ��� . ��� ��� ��� �������� ������� ������� ����� �������� ����� .<p>

��� ������ ����� ��� ��� ����� "manifest.php" ������� ��� (777) ����� ���� ������� � ������� . ���� �� ������ ����� �������� .<p>

<b>������ ������� ����� ��� ��� ������</b><br>
���� �� ���� �� ��� ��� ������� . ��� �� ��� ���� ��� ����� , ���� ������� ( ���� ��� ������ ) �� April 13, 2005, ����� 041305 (mmddyy) .

���� ������� ���� �� ��� � ���� ��� �������  ��� ������� ($FILE_location_filename, � ����� ����� , ����� "addadmin.php" �����

"$FILE_admin_addadmin") , �� ��� ������ �� ����� "manifest.php" � ���� ������� �������� , ��� ���� ��� XML  ��� ���� eschew.net .<p>

������� �������� ����� ���� ����� ��������� ������� ������� , ��� ����� ���� ���� ��� ���� , ��� ��� ������� �� ������� �������� ��� ������ ����� , � ������ �� ������� �������� , � ����� ����� ��������� ��� ��� XML , �� ��� ������ ����� ��������� �� ������� �������� �� �������� �� ����� ����� manifest . ��� ���� ������ , ����� ���� ������ ������� �� ������� ����� ��� ��� .txt . ��� ��� ���� ������� , ��� ���� ������� ���� �� �� .<p>

��� ���� ��� ����� ���� , ���� ��� ���� ����� ����� ������ � ���� "��� ����� ����..." �� ��� ����� ����� ����� �� .txt ��� .php , �� �� ���� ������� ������� ��� ����� , �� ��� ��� ����� ������ , �� �� ������ ����� ������� .
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="pause">����� ���� �������</a></div><p>
���� �� �� ���� �� ����� ���� ����� �������� ���� ������ , �� ��� ������ ��� ��� �������� ���������� � ������ �������� ���������� , ����� ������ ������� �� ��� ������ .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>


<div class="lefthead"><a NAME="commerce">��� ������</a></div><p>
��� ���� ���� ������ ������� , ����� ��� ��� �� ������� phpBannerExchange RC3 � ������ . ��� ����� ������ ���� PayPal ��� ���� ������ �� ������� . ��� ������ ������ ���� PayPal IPN ������ �� ������� � ��� ������ ���� �����.<p>
�������� ���� ��� ������ ����� �� ���� ��� ����� ����� ������� ����� ���������� . " ���� ��� �������� ������ ���������� ������ ��������" .<p>
�� �� ����� ������ �� ����� "paypal.config.php" �� ������ "/lib/commerce/" . ����� ��������� ������� �<p>
<table class="tablebody" width="100%">
<tr>
<td valign="top" width="130">
<b>$businessname �</b></td><td>����� �� paypal ������ ���������� .</td>
</tr>
<tr>
<td valign="top">
<b>$ipn_page �</b></td><td>�� ��� ����� ��� ������ ��� ��� . ��� ���� ��������� �� �������� , ����� ���� �������� ������ �� ������� .</td>
</tr>
<tr>
<td valign="top">
<b>$propername �</b></td><td>��� ������ ���� �������� . ������ ������ "PayPal" ��� ���� ������ �������� .</td>
</tr>
<tr>
<td valign="top">
<b>$payment_currency �</b></td><td>��� ������ �������� �� PayPal . ��� ������ ������� ������ � ������� �������� US .</td>
</tr>
<tr>
<td valign="top">
<b>$currency_sign �</b></td><td>��� ������ ���� ���� ���������� . ( ������ : ������ ������ ������� (\) ��� ������� ��� ������� ($) ) .</td>
</tr>
<tr>
<td valign="top">
<b>$currency_int �</b></td><td>��� ������� ������� �� ������ . ���� , 25 us ����� ������ ���� ���� ���� $25 USD .</td>
</tr>
<tr>
<td valign="top">
<b>$decimal_separator �</b></td><td>��� ������� ���� ���� ��������� ������ ����� , ���� ���� ( . ) , ���� ���� ������� ������� ( , ) .</td>
</tr>
<tr>
<td valign="top">
<b>$thousands_separator �</b></td><td>��� ������� ���� ���� ��������� ������ ����� , ���� ���� ( , ) , ���� ���� ������� ( . ) .</td>
</tr>
<tr>
<td valign="top">
<b>$places �</b></td><td>��� ������ ��������� "���" . �� ���� ������� �������� , ���� 2 ���� .</td>
</tr>
</table>
<p>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="nav">���� ������</a></div><p>
������ ������ ��� �������� .<p>
����� ��� ������<b>������ ��������</b> ����� ��� ���� ���������� .<p>
����� ��� ������ <b>����� ������</b> ������ ������ �� ���� ������ , � ����� ���� ����� . �� ����� ��� ��� �� ��� ���� ���� �� ���� ������ .<p>
����� ��� ������ <b>��������</b> ����� ��� ��� ������ .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>

<div class="lefthead"><a NAME="gethelp">������ ��� ��������</a></div><p>
�� ����� ��� ���� ���� �������� . � �� ���� ��� ��� �� ��� ���� ������ ����� ����� �������� . ����� ���� ����� ��������� ������ � ������ ���������� ������� .<p>
��� ����� �� ����� �� ��� �������� , � ����� ��� �������� , ����� ������ ��� <a href="http://www.eschew.net/forums/">������� ����� �����</a> � ��� ������ .<p>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead">������� �� �������</div><p>
��� ����� �� ���� �� �������� , ������ ������ ���� . ��� ����� ��� � ��� ���� ���� �� ������� �� �� ����� �� ����� , ����� ���� . � �� ��� �� ������ ���� �� �� ����� . ��� ���� ��������� ��� ����� �� ����� , ������ ��� ��� �� <a href="http://www.eschew.net/forums/">������� ����� �����</a> . � ���� �� ��� ������ ���� ������� �������� , ������ ����� , �� ����� ������ , �� ������ ���� ������� � ���� . ( ������ ������� ) .<p>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead">���� �����</div><p>
��� ������ �� ����� ����� ����� , ������ ������ ��������� ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> ��� ������ ��� ����� �� ��������� ��� ���� � ����� . ��� ������� �� ������� ����� ���� ��� ������ ����� � ���� ����� ������ �� ����� ����� �� ���� ����� ������� , ������ ����� ����� �� ������ �������� ��������� ��� , � �� ����� ������ ���� ������� . � �� ��� ���� ����� . ( ����� ������� ) .<p>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
<div class="lefthead">������� ������� � �������</div><p>
��� ��� ���� ��� ��������� ��� �������� �� ���� ����� ���� , ���� ����� ��� �������� �� ���������� ������� . ���� ������ ��� ��������� � ������ �� ���� ���� ��� ��� ������� . ����� ����� ���� ��� ��������� � �������� ����� ������� ������� add-on ��� �������� ������� . ��� ���� ������� �� ������� ������ , ����� ����� ��������� ��� ��� ������ ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> .<p>
����� ����� �������� ��� ���� ������ . ���� ����� ����� ������ �� ������ <b>lang/</b> ��� ������ ����� �������� banner exchange . ��� ��� ������ ������� , ���� ������ ����� ���� � ����� ������ ����� ����� ������� . ����� ����� ����� ����� �������� ��� ������ ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> .<p>
������� ������ �� ������ <b>template/</b> . ��� ��� ������ ������ �� ��� ���� ������� ��������� templates �� style.css , ���� ������ ����� ���� . ����� ����� ������� ��� ������ ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> .<br>
<p>
[<a href="admin.php#top"> ������ </a>]<p>
<hr>
<p>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</div>
<center>
<div class="footer"><a href="http://www.eschew.net/scripts/">phpBannerExchange 2.0</a> is an original work and is Copyright &copy; 2002-2005 by eschew.net Productions. phpBannerExchange 2.0 is distributed at no charge in accordance with the <a href="http://www.gnu.org/licenses/lgpl.html">GNU General Public License</a> (GPL). Removal of any copyright notices from this script or any of the files included with this script constitutes a violation of this license. Please do not steal my work. <br>Translation into Arabic by <a title="����� ��� ����" target="_blank" href="http://www.sakrkuraish.netfirms.com">Sakr Kuraish</a>
<? include("adminmenu.php"); ?>