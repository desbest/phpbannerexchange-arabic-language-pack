<? include ("../css.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="rtl">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1256">
<meta http-equiv="Content-Language" content="ar-sy">
<title>����� �������� ������� phpBannerExchange 2.0</title>
<link rel="stylesheet" href="../template/css/<? echo "$css"; ?>" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0"
  marginheight="0" >
<div id="content">
<div class="main">
<table border="0" cellpadding="1" width="100%" cellspacing="0">
<tr>
<td>
<table cellpadding="5" border="1" width="100%" cellspacing="0">
<tr>
<td colspan="2" class="tablehead"><center><div class="head">����� �������� ������� phpBannerExchange 2.0</center></div></td>
</tr>
<td class="tablebody" colspan="2">
<div class="mainbody">
<table border="0" cellpadding="1" cellspacing="1" style="border-collapse: collapse"  width="90%">
  <tr>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="90%" >
<tr>
<div class="lefthead"><a NAME="new">�� ������</a></div>
<ul>
<li>����� ���� ���� ����� ��������</li>
<li>��� �����</li>
<li>���� ����� �������� � � ��� �������</li>
<li>������ ��������� ������ ���� ������</li>
<li>����� ����������</li>
<li>��� �������</li>
<li>����� ������ �� ����� ����� ������� ��������</li>
<li>��� ������� ������ ( ����� ���� )</li>
<li>���� ��� ������ ���� ���������</li>
<li>����� ����� ������ ����</li>
<li>����� ������ ������ ���� ����</li>
<li>���� ������ �������</li>
<li>��� ���� MD5 ������ ����� ������</li>
<li>��� ��� ��������</li>
<li>����� ��� �������� ���������</li>
<li>� ������ �� �������� ������ ...</li>
</ul>
[<a href="install.php#top">top</a>]<p>
<div class="lefthead"><a NAME="quickstart">���� ������� ������� ( ����� ���� )</a></div>
��� ��� ���� �������� �� ������� phpBannerExchange 1.x , ���� ����� ������� <a href="install.php#upgrading">��������</a> ���� ��� ������ .<ol>
<li>������ Unzip ������� �� ����� ������� .</li>
<li>���� ���� �������� ��� ����� . �� ���� ����� ������ binary , ���� ���� ��� ������� ������ ASCII .</li>
<li>���� ����� config.php ������� ( 755 �� 777 ) ������ ����� ���� ����� / ����� .</li>
<li>���� ����� manifest.php ������� ( 755 �� 777 ) ������ ����� ���� ����� / ����� .</li>
<li>���� ����� css.php ������� ( 755 �� 777 ) ������ ����� ���� ����� / ����� .</li>
<li>���� ���� �������� ������� ( ����� ) ������� ( 777 ) ������ ����� ���� ����� / ����� .</li>
<li>���� ������  template  ������� ( 777 ) ������ ����� ���� ����� / ����� .</li>
<li>����� ������ <b>admin/db</b> ������ ����� ����� / ����� , � �� ������� ����� ���� �������� ����� .htaccess .</li>
<li>���� ��������� ��� <i>http://yourdomain.com/exchange_directory/install/install.php</i> .</li>
<li>���� ��������� ������� �� ����� ������� .</li>
<li><b>���� ���� ������� , ������ INSTALL ��� ���� ����� ������� .</b></li></ol>
[<a href="install.php#top">top</a>]<p>
<div class="lefthead"><a NAME="detailed">Install.php</a></div>
������ phpBannerExchange 2.0 ���� �� ����� ����� ������ ����� �� ����� ��������� , ����� ������� � ����� ���� ������ ������� �������� . ��� ����� ����� ������� , ���� �� ��� ��� ���� ���� ����� �������� ��� ����� ��� ���� ��� �� � �� ����� �� �� ����� ���� .<p>

������ ����� ������� , ���� ��� <i>http://www.yourdomain.com/root_bannerexchange_directory/install/install.php</i> ������ �������� . ������ "yourdomain.com" �������� ����� �� , � "root_bannerexchange_directory" ���� ������ ����� ���� ���� �������� ���� ������ ( ���� � http://www.eschew.net/exchange/install/install.php ).<p>

������ ��� ����� ������� ��� ����� � ������ ������ . ��� �� ����� ������� ����� �������� ��� "config.php" , "css.php" , � "manifest.php" �

���� ������� template , ��� ��� ������ �� ������ ������ ������� ������ ������� ����� . ��� ��� �������� "chmod" ������ �������� . ���� ����� FTP ���� �� ������ ������� ������ ����� ��� ����� ����� ������ ��� ����� . �� ������� ��� "755" . �� DOS/*nix FTP , ������ ������� ������� �<p>

<i>���� ������ ����� 755 config.php</i><p>

��� ��� ���� ������ ������� ���� �������� ��� ����� , ������ ����� ��� ����� ���� ( ���� ) ����� ����� ����� (  ���  "banners" ) � ���� ������� "777" . ������ admin/db ����� ��� ������� "777" ����� . ��� ������ ������ ��� ����� ����� ��������� ���� ������� �� �������� .<p>

<table class="tablebody" width="200" dir="ltr">
<tr>
<td valign="top" align="left"><b>�����/������</b></td><td dir="rtl"><b>������� �������</b></td>
</tr>
<tr>
<td valign="top" align="left">/config.php</td><td dir="rtl">755 �� 777</td>
</tr>
<tr>
<td valign="top" align="left">/manifest.php</td><td dir="rtl">755 �� 777</td>
</tr>
<tr>
<td valign="top" align="left">/css.php</td><td dir="rtl">755 �� 777</td>
</tr>
<tr>
<td valign="top" align="left">/templates/*</td><td dir="rtl">777</td>
</tr>
<tr>
<td valign="top" align="left">/admin/db/</td><td dir="rtl">777</td>
</tr>
<tr>
<td valign="top" align="left">/upload_dir/</td><td dir="rtl">777</td>
</tr>
</table>
<p>
[<a href="install.php#top"> ������ </a>]<p>

<div class="lefthead"><a NAME="vars">���������</a></div>
��� ����� ��������� ���� ����� ��� ���� ����� ������� �� ��� ���� ��� ���� .<p>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="tablebody" width=150 valign="top"><b>�����</b></td><td class="tablebody"><b>���� config.php</b></td><td class="tablebody"><b>�����</b></td>
</tr>
<tr>
<td class="tablebody" width=150 valign="top">���� ����� �������� �</b></td><td class="tablebody" valign="top" dir="ltr">$dbhost</td><td class="tablebody">��� ������ ����� ������� . ������ ���� <i>localhost</i>.</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$dblogin</td><td class="tablebody">��� �������� ������� ������ �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ������ �</td><td class="tablebody" valign="top" dir="ltr">$dbpass</td><td class="tablebody">���� ������ ������� ������ �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ����� �������� �</td><td class="tablebody" valign="top" dir="ltr">$dbname</td><td class="tablebody">��� ����� ������� ���� ���� ��������� ������ ������ �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$baseurl</td><td class="tablebody">���� �������� �������� ��� �������� . ���� , ��� ��� ������ �������� ��� ������ "exchange" ���� ������ ��� <i>http://www.yourdomain.com/exchange</i> . ���� ���� ����� �� ������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$basepath</td><td class="tablebody">������ ( <b>� ���</b> ������ ) �� ���� �������� �� ����� . ��� ��� ��� ������ ���� ������ ������ ����� ������� �� ������� . �� �� ��� ��� �������� , ���� ������ ������ �����  ( ���� � <i>/home/www/exchange</i> ) . ���� ���� ����� ��� ��� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$exchangename</td><td class="tablebody" valign="top">��� �������� ���� ������ ������� � �� ���� ������ � ����� ������ ���������� ��� .  ���� � ���� ����� ��������� ) .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �</td><td class="tablebody" dir="ltr">$sitename</td><td class="tablebody">��� ������ ������� , ������ �� ������ � ���� ������� ( ���� � ��� ���� ) .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �</td><td class="tablebody" valign="top" dir="ltr">$adminname</td><td class="tablebody">���� �� ��� ������ . ������ ���� ����� ���� �� ���� , ����� ����� ������ ������ ���������� ������� ����� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ������ �<td class="tablebody" valign="top" dir="ltr">$ownermail</td><td class="tablebody">����� ������ ���������� ���� ���� �������� ����� ������� � ������� �� �������� ����� ��� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������� �</td><td class="tablebody" valign="top" dir="ltr">$bannerwidth</td><td class="tablebody">��� ������� ������� �� ������� , �������� �������� ���� ���� 468 ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ������� </td><td class="tablebody" valign="top" dir="ltr">$bannerheight</td><td class="tablebody">������ ������� ������� �� ������� , �������� �������� ���� ������� 60 ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ������� ���������� �</td><td class="tablebody" valign="top" dir="ltr">$steexp, $banexp</td><td class="tablebody">����� ���� ������� ���������� ������� ���� ���� ���� ������� �� ���� ����� . �� ����� ����� ��� ����� �� ��� ��� ���� �������� �� ���� ����� ����� ��� ����� ����� �� ���� ��� ���� ���� . ���� � ������ "3" ���� ��� ����� ����� ���� ����� ����� ���� ���� �������� �� ����� . ��� ����� ������ ��� ������ ��� ������ ���� ����� �� ���� ����� ���� ��� ������ ���� ����� . ���� � ������ "3" ���� ��� 3 ���� ��� ��� . �� ������ ��� ��� ����� ������� ��������� �<p>
<b>1</b> ��� ���� �� ���� ����� ����� � <b>1</b> ��� ������ ����� � <b>1 ����� 1</b><br>
<b>2</b> ��� ���� �� ���� ����� ����� � <b>1</b> ��� ������ ����� � <b>1 ����� 2</b><br>
<b>3</b> ��� ���� �� ���� ����� ����� � <b>2</b> ��� ������ ����� � <b>1 ����� 1.5</b></td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$showimage</td><td class="tablebody">��� ����� ����� ��� �� ��� ���� ���� ���� ����� �������� ����� ������� . ���� ��� ���� ����� 60X60 ������� ��� 468X60 ���� . ����� ���� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$imagepos</td><td class="tablebody">��� ���� ��� ���� �������� �� ������ ������ , ����� ��� ����� �������� ������� ������� , ���� , ���� , ���� , �� ���� ������� . ��� ����� ���� �� ���� ��� ������ ������ ���� ����� (468X60) ���� , ��� ��� ��� ������� ������� ��� 468 ���� . </td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$imageurl</td><td class="tablebody">��� ���� ��� ���� �������� �� ������ ������ , ���� ��� ������ ������ ������ (���� � <i>http://www.yourdomain.com/image.gif</i>) . � ��� ����� ��� ����� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$showtext, $exchangetext</td><td class="tablebody">��� ��� ���� ���� ���� ���� ��� �������� ���� ������� , �� ��� ������ ��� "���" , �� ���� ���� ���� ���� ���� ������ �� ��� ���� . ( ���� � ���� �� ����� ������ �� "��� �������� " ) �� �� �� ��� ������ .
</td>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� ��� ������� �</td><td class="tablebody" valign="top" dir="ltr">$reqbanapproval</td><td class="tablebody">��� ������ ����� �� ������ �������� � �������� ����� ��� �� ��� ������� �� ������� . ����� ������ ��� <b>��</b> ���� ������ ��� �������� ��� ������ � ���� ������ ��� . ��� ����� ������ ��� <b>���</b> ����� ��� ������ �� ����� ������� �������� ���� ��� ����� ������ �� ����� �� ��� ������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$allow_upload</td><td class="tablebody">��� ������ ����� �� ����� ���� ������ ������ ������� ��� . ����� ��� ������ ��� <b>��</b> ���� ��� ��� ����� ������� ����� ����� ��� ����� . � ������� ���� ��� ���� �� ������� ���� ������� � ������� �������� �� ��� ��� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ ����� �</td> <td class="tablebody" valign="top" dir="ltr">$max_filesize</td><td class="tablebody">����� ����� ������ ���� ���� ������� ������� ������ "�������" . ���� � ( 30 ���� ��� ������� "30000" ) . ����� ��� ��� ����� ���� �� ��� �� ��� ������ ���� ����� ��� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� ��� �������� �</td><td class="tablebody" valign="top" dir="ltr">$upload_path</td><td class="tablebody">���� ���� ������� �� *nix/Windows ���� ���� �� ��� <i>/home/username/public_html/exchange/upload</i> �� �� ���� . "���� ���� ����� ��� ������" ��� ������ ��� �� ���� ��� ���� ������� � ������� ( ��� ���� ������� ������� 777 ) . ���� ����� ����� �� �� ��� ����� �� ���� html . ����� ��� ��� ����� ���� �� �� ��� ������ ���� "����� ��� ��������" .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">���� ���� �������� �</td><td class="tablebody" valign="top" dir="ltr">$banner_dir_url</td><td class="tablebody">��� ����� ���� "������" ��� ���� ������� ������ ������ �� ������ ������ . ���� <i>http://www.yourdomain.com/exchange/upload</i> "���� ���� ����� �� �������" ����� ��� ��� ����� ���� �� �� ��� ������ ���� "����� ��� ��������" .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� �������� ������ �</td><td class="tablebody" valign="top" dir="ltr">$maxbanners</td><td class="tablebody">��� ����� ����� ����� ������ �������� ���� ���� ����� �������� ��� ����� . ��� ��� ���� ����� ������ �������� ��� ��� ����� �� �������� , �� ������ ��� "0" (zero) . ��� ������ ���� �� ��� ������� ( ��� �������� �� ��� ���� ��� ���� ������ ) .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ ���� �</td><td class="tablebody" valign="top" dir="ltr">$anticheat</td><td class="tablebody">����� ����� ��� ���� ���� ���� ��������� . �������� ������� <b>����� ��������</b> , <b>�������</b> , � <b>����</b>. <p>
������ <b>�������</b> ����� ��� ������� ������ �� ���� �������� ���� ������ ������ ����� . ����� ��� ��� ������ ����� ������� ��� ������ ����� ������ ��� ���� ����� . ( ����� ����� ��� �� ������ ������ ) .
<p>
������ <b>����� ��������</b> ����� ����� ������� � ��� ��� ��� ����� ��������� �� ����� �������� , ��� ��� ������� ������� , � �� ���� ��� , � ��� ��� ��� ��� ������� ���� ���� ���� ������ �� ��� ����� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ �</td><td class="tablebody" valign="top" dir="ltr">$expiretime</td><td class="tablebody">��� ��� ����� ����� �������� ���� ��������� ��������� ���� ���� . � �� ������ ������ ���� ����� ��� ������ ������ ��� ��� ����� ������ ���� �������� . ���� ������ ��� ����� , ��� <b> 20 �� 30  ����� </b> .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ������� �</td><td class="tablebody" valign="top" dir="ltr">$referral_program</td><td class="tablebody">��� ������ ����� �� ��� ������� ���� ������ ��� ����� ��� ���� ������ ����� ��� ����� �������� �������� �� ���� ����� , ���� � (<i>http://www.somesite.com/exchange/index.php</i>) � ���� ���� �� ���� ��� ��� ���� ���� ����� . ���� ����� ��������� �� ������� ��� ����� �� ���� ��� ���� , ��� ������ ������ ��� ��� ����� ������ ������ � �������� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������ ������� �</td><td class="tablebody" valign="top" dir="ltr">$referral_bounty</td><td class="tablebody">��� ������ ������ ��� ������ ���� ���� ����� ����� ����� �� ����� ������ ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �� �</td><td class="tablebody" valign="top" dir="ltr">$startcredits</td><td class="tablebody">��� ���� ���� ��� �� ������ �������� ����� ������ ��� ������ , ���� ����� ���� ���� . ����� ����� ��� ������ ��� ���� ���� � ���� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ������ �</td><td class="tablebody" valign="top" dir="ltr">$sellcredits</td><td class="tablebody">����� ���� ��� ������ . ��� ������ ���� ���� ������� ��� �� ���� ������� , ��� �� ���� ��� ���� ������ . ���� ��� ����� ������� ����� �� ��������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">��� ���� X  ���� �</td><td class="tablebody" valign="top" dir="ltr">$topnum</td><td class="tablebody">����� ��� "���� ��������" �� ��� ������ , ���� ����� �� ���� ����� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ �</td><td class="tablebody" valign="top" dir="ltr">$sendemail</td><td class="tablebody">��� ���� ����� ����� ������� ���������� ��� ����� ����� ���� ��� ������ "���" .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������� ���� MD5 ������ ����� ������ �</td><td class="tablebody" valign="top" dir="ltr">$usemd5</td><td class="tablebody">������ ����� , ����� ����� ����� ������ �������� ���� MD5 , ��� ��� ����� ���� ������ ��� "32" ���� . ���� �� ���� ������ ����� ��� ����� ������� ( ������ ����� ���� ) ��� ������� ��� ������ .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������� ��� gZip/Zend �</td><td class="tablebody" valign="top" dir="ltr">$use_gzhandler</td><td class="tablebody">��� ��� ���� ���� ������� � ����� ������ ���� gZip , �� ������ ��� ������ . ��� ������ ���� �� ������� ���� ������� , ����� ���� ������ ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� ������ �������  �</td><td class="tablebody" valign="top" dir="ltr">$log_clicks</td><td class="tablebody">����� ��� & ����� � ����� IP �� ���� ������� �� ����� �������� . ���� ��� ��� ��� ��� ������� ���� ���� , ������ ����� ����� �������� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">������� ���� mySQL4 rand() �</td><td class="tablebody" valign="top" dir="ltr">$use_dbrand</td><td class="tablebody">����� ����� mySQL rand() �� ��������� ����� view.php . ��� ������ ���� ���� ���� �� ���� ���� �������� �� ����� ������ ��������� , ����� ����� �� ���� ���� ����� mySQL 4.x �� ���� .</td>
</tr>
<tr class="tablebody">
<td class="tablebody" width=150 valign="top">����� �������  �</td><td class="tablebody" valign="top" dir="ltr">$date_format</td><td class="tablebody">����� ��� ����� ������� �������� �� �������� .  ( mm/dd/yyyy �� dd/mm/yyyy ) .</td>
</tr>
<table>

[<a href="install.php#top"> ������ </a>]<p>

<div class="lefthead"><a NAME="dbstructure">���� ����� ��������</a></div>
��� ����� ���� ����� �������� .<p>
<pre dir="ltr">
--
-- Table structure for table `banneradmin
--

CREATE TABLE banneradmin (
  id int(11) NOT NULL auto_increment,
  adminuser varchar(15) NOT NULL default '',
  adminpass varchar(255) NOT NULL default '',
  PRIMARY KEY  (id),
  UNIQUE KEY id (id,adminuser)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannercats`
--

CREATE TABLE bannercats (
  id int(7) NOT NULL auto_increment,
  catname varchar(50) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerclicklog`
--

CREATE TABLE bannerclicklog (
  id int(11) NOT NULL auto_increment,
  siteid int(11) NOT NULL default '0',
  clickedtosite int(11) NOT NULL default '0',
  bannerid int(11) NOT NULL default '0',
  ip varchar(255) NOT NULL default '',
  page int(11) NOT NULL default '0',
  time int(11) NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannercommerce`
--

CREATE TABLE bannercommerce (
  productid int(11) NOT NULL auto_increment,
  productname text NOT NULL,
  credits decimal(14,0) NOT NULL default '0',
  price decimal(12,2) NOT NULL default '0.00',
  purchased int(11) NOT NULL default '0',
  UNIQUE KEY productid (productid)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerconfig`
--

CREATE TABLE bannerconfig (
  name varchar(255) NOT NULL default '',
  data longtext NOT NULL,
  PRIMARY KEY  (name)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerfaq`
--

CREATE TABLE bannerfaq (
  id int(11) NOT NULL auto_increment,
  question longtext NOT NULL,
  answer longtext NOT NULL,
  UNIQUE KEY id (id)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerlogs`
--

CREATE TABLE bannerlogs (
  uid int(11) NOT NULL default '0',
  ipaddr text NOT NULL,
  page int(11) NOT NULL default '0',
  timestamp text NOT NULL
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerpromologs`
--

CREATE TABLE bannerpromologs (
  id int(11) NOT NULL auto_increment,
  uid int(11) NOT NULL default '0',
  promoid int(11) NOT NULL default '0',
  usedate int(11) NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerpromos`
--

CREATE TABLE bannerpromos (
  promoid int(11) NOT NULL auto_increment,
  promoname varchar(255) NOT NULL default '',
  promocode varchar(255) NOT NULL default '',
  promotype int(11) NOT NULL default '0',
  promonotes text,
  promovals decimal(11,2) NOT NULL default '0.00',
  promocredits int(11) NOT NULL default '0',
  promoreuse tinyint(4) NOT NULL default '0',
  promoreuseint int(11) NOT NULL default '0',
  promousertype tinyint(4) NOT NULL default '0',
  ptimestamp int(11) NOT NULL default '0',
  promostatus tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (promoid)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerrefs`
--

CREATE TABLE bannerrefs (
  id int(11) NOT NULL auto_increment,
  uid int(11) NOT NULL default '0',
  refid tinyint(4) NOT NULL default '0',
  given tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannersales`
--

CREATE TABLE bannersales (
  invoice int(11) NOT NULL default '0',
  uid int(11) NOT NULL default '0',
  item_number int(11) NOT NULL default '0',
  payment_status text NOT NULL,
  payment_gross text NOT NULL,
  payer_email varchar(200) NOT NULL default '',
  timestamp int(14) NOT NULL default '0'
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerstats`
--

CREATE TABLE bannerstats (
  uid int(11) NOT NULL default '0',
  category int(11) NOT NULL default '0',
  exposures int(11) NOT NULL default '0',
  credits int(11) NOT NULL default '0',
  clicks int(11) NOT NULL default '0',
  siteclicks int(11) NOT NULL default '0',
  approved tinyint(4) NOT NULL default '0',
  defaultacct tinyint(4) NOT NULL default '0',
  histexposures int(11) NOT NULL default '0',
  raw tinyint(4) NOT NULL default '0',
  startdate int(11) NOT NULL default '0',
  PRIMARY KEY  (uid)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `bannerurls`
--

CREATE TABLE bannerurls (
  id int(11) NOT NULL auto_increment,
  bannerurl varchar(200) NOT NULL default '',
  targeturl varchar(255) NOT NULL default '',
  clicks tinyint(4) NOT NULL default '0',
  views int(11) NOT NULL default '0',
  uid int(11) NOT NULL default '0',
  pos int(11) NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `banneruser`
--

CREATE TABLE banneruser (
  id int(11) NOT NULL auto_increment,
  login varchar(20) NOT NULL default '',
  pass varchar(255) NOT NULL default '',
  name varchar(200) NOT NULL default '',
  email varchar(100) NOT NULL default '',
  newsletter tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (id),
  UNIQUE KEY id (id,login)
) TYPE=MyISAM;

</pre>
[<a href="install.php#top"> ������</a>]<p>

<div class="lefthead"><a NAME="upgrading">�������</a></div>
����� �������� ������� ���� �� ������ . ����� ������� ���� �� ������ /install , ��� ����� ������� �������� � ����� ��� config.php .<p>

<div class="lefthead">������� ������� �� ������� 1.x ��� 2.0</div>
<ol>
<li>�� ���� ��� ������� ������ �������� � ����� �������� . ��� ����� ��� ��� �� �� ���� ������� .</li>
<li>�� ���� ��� ����� �������� ��� ����� ������ ASCII .</li>
<li>���� ����� config.php ������� ( 755 �� 777 ) ����� ���� ������ � ������� .</li>
<li>���� ����� manifest.php ������� ( 755 �� 777 ) ����� ���� ������ � ������� .</li>
<li>���� ����� css.php ������� ( 755 �� 777 ) ����� ���� ������ � ������� .</li>
<li>���� ���� ����� �������� ������� ( 777 ) ������ ������ ���� ����� � ����� .</li>
<li>���� ������ template ������� ( 777 ) ������ ������ ���� ����� � ����� .</li>
<li>��� ������� ��� <i>http://yourdomain.com/exchange_directory/install/install.php</i> .</li>
<li>��� ���� ������� " �� 1.x ��� 2.0 " �� ���� ��������� ������� �� ����� ������� . ��� ����� �������� ��� ��� ��������� , <a href="install.php?#vars">���� ������</a> .</li>
<li><b>��� ���� ����� ������� �� ���� ������ INSTALL ����� .</b></li></ol>

<div class="lefthead">������� ������� �� ������� 2.0 RCx ��� 2.0 Gold</div>
<ol>
<li>�� ���� ��� ������� ������ �������� � ����� �������� . ��� ����� ��� ��� �� �� ���� ������� .</li>
<li>�� ���� ��� ����� �������� ��� ����� ������ ASCII .</li>
<li>���� ����� config.php ������� ( 755 �� 777 ) ����� ���� ������ � ������� .</li>
<li>���� ����� manifest.php ������� ( 755 �� 777 ) ����� ���� ������ � ������� .</li>
<li>���� ����� css.php ������� ( 755 �� 777 ) ����� ���� ������ � ������� .</li>
<li>���� ���� ����� �������� ������� ( 777 ) ������ ������ ���� ����� � ����� .</li>
<li>���� ������ template ������� ( 777 ) ������ ������ ���� ����� � ����� .</li>
<li>��� ������� ��� <i>http://yourdomain.com/exchange_directory/install/install.php</i> .</li>
<li>���� ��������� ������� �� ����� ������� , ��� ����� �������� ��� ��� ������� , <a href="install.php?#vars">���� �������</a> .</li>
<li><b>��� ���� ����� ������� �� ���� ������ INSTALL ����� .</b></li></ol>
[<a href="install.php#top"> ������</a>]<p>

<div class="lefthead"><a NAME="gethelp">������ ��� ��������</a></div>
�� ���� ��� ���� �������� , ��� ���� ��� ��� �� ���� �� ����� ����� ����� �������� . ����� ���� ����� ����� �������� ������� ���� ��� � ����� ��������� ���� ( ������ ������� ) .<p>
��� ����� �� ����� �� ����� �������� , � ����� �������� , ����� ������ ��� <a href="http://www.eschew.net/forums/">������� ����� �����</a> � ��� ������ .<p>
<div class="lefthead">������� �� �������</div>
��� ����� �� ���� �� �������� , ������ ������ ���� . ��� ����� ��� � ��� ���� ���� �� ������� �� �� ����� �� ����� , ����� ���� . � �� ��� �� ������ ���� �� �� ����� . ��� ���� ��������� ��� ����� �� ����� , ������ ��� ��� �� <a href="http://www.eschew.net/forums/">������� ����� �����</a> . � ���� �� ��� ������ ���� ������� �������� , ������ ����� , �� ����� ������ , �� ������ ���� ������� � ���� . ( ������ ������� ) .<p>
<div class="lefthead">���� �����</div>
��� ������ �� ����� ����� ����� , ������ ������ ��������� ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> ��� ������ ��� ����� �� ��������� ��� ���� � ����� . ��� ������� �� ������� ����� ���� ��� ������ ����� � ���� ����� ������ �� ����� ����� �� ���� ����� ������� , ������ ����� ����� �� ������ �������� ��������� ��� , � �� ����� ������ ���� ������� . � �� ��� ���� ����� . ( ����� ������� ) .<p>
<div class="lefthead">������� ������� � �������</div>
��� ��� ���� ��� ��������� ��� �������� �� ���� ����� ���� , ���� ����� ��� �������� �� ���������� ������� . ���� ������ ��� ��������� � ������ �� ���� ���� ��� ��� ������� . ����� ����� ���� ��� ��������� � �������� ����� ������� ������� add-on ��� �������� ������� . ��� ���� ������� �� ������� ������ , ����� ����� ��������� ��� ��� ������ ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> .<p>
����� ����� �������� ��� ���� ������ . ���� ����� ����� ������ �� ������ <b>lang/</b> ��� ������ ����� �������� banner exchange . ��� ��� ������ ������� , ���� ������ ����� ���� � ����� ������ ����� ����� ������� . ����� ����� ����� ����� �������� ��� ������ ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> .<p>
������� ������ �� ������ <b>template/</b> . ��� ��� ������ ������ �� ��� ���� ������� ��������� templates �� style.css , ���� ������ ����� ���� . ����� ����� ������� ��� ������ ���������� <a href="mailto:darkrose@eschew.net">darkrose@eschew.net</a> .<br>
[<a href="install.php#top"> ������ </a>]<p>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</div>
<center>
<div class="footer"><a href="http://www.eschew.net/scripts/">phpBannerExchange 2.0</a> is an original work and is Copyright &copy; 2002-2005 by eschew.net Productions. phpBannerExchange 2.0 is distributed at no charge in accordance with the <a href="http://www.gnu.org/licenses/lgpl.html">GNU General Public License</a> (GPL). Removal of any copyright notices from this script or any of the files included with this script constitutes a violation of this license. Please do not steal my work.<br>Translation into Arabic by <a title="����� ��� ����" target="_blank" href="http://www.sakrkuraish.netfirms.com">Sakr Kuraish</a>
<? include("installmenu.php"); ?>